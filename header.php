<!DOCTYPE html>
<!-- Open Html -->
<html <?php language_attributes(); ?>>
<!-- Open Head -->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="google-site-verification" content="tRgBrPOb1R3YtGQxCybJwkvnS3Z57OgazUU6-bqWRPg" />
	<link rel="alternate" href="http://viettitan.com/" hreflang="vi-vn" />

	<?php wp_head(); ?>
</head>
<!-- Close Head -->
<body <?php body_class(); ?>>
<?php
/**
 * @hooked  - g5plus_site_loading - 5
 **/
do_action( 'g5plus_before_page_wrapper' );
?>
<!-- Open Wrapper -->
<div id="wrapper">
	<?php
	/**
	 * @hooked - g5plus_page_above_header - 10
	 * @hooked - g5plus_page_top_bar - 15
	 * @hooked - g5plus_page_header - 20
	 **/
	do_action( 'g5plus_before_page_wrapper_content' );
	?>

	<!-- Open Wrapper Content -->
	<div id="wrapper-content" class="clearfix">

<?php
/**
 **/
do_action( 'g5plus_main_wrapper_content_start' );
?>



