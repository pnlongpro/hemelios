<?php
$g5plus_options = g5plus_option();

$footer_layout = get_post_meta( get_the_ID(), 'g5plus_footer_layout', true );

if ( !isset( $footer_layout ) || $footer_layout == '-1' || $footer_layout == '' ) {
	$footer_layout = $g5plus_options['footer_layout'];
}
$top_footer_layout = get_post_meta( get_the_ID(), 'g5plus_top_footer_layout', true );

if ( !isset( $top_footer_layout ) || $top_footer_layout == '-1' || $top_footer_layout == '' ) {
	$top_footer_layout = $g5plus_options['top_footer_layout'];
}


$col_top_footer = 0;
for ( $i = 1; $i <= 4; $i ++ ) {
	if ( is_active_sidebar( 'footer-' . $i ) ) {
		$col_top_footer += 1;
	}
}
$col_top_footer_class = array();

if ( $top_footer_layout == 'footer-1' ) {
	$col_top_footer_class[0] = $col_top_footer_class[1] = $col_top_footer_class[2] = $col_top_footer_class[3] = 'col-md-3 col-sm-6';
}
if ( $top_footer_layout == 'footer-2' ) {
	$col_top_footer_class[0] = 'col-md-6 col-sm-12';
	$col_top_footer_class[1] = $col_top_footer_class[2] = 'col-md-3 col-sm-6';
}

if ( $top_footer_layout == 'footer-3' ) {
	$col_top_footer_class[0] = $col_top_footer_class[1] = 'col-md-3 col-sm-6';
	$col_top_footer_class[2] = 'col-md-6 col-sm-12';
}

if ( $top_footer_layout == 'footer-4' ) {
	$col_top_footer_class[0] = $col_top_footer_class[1] = 'col-md-6 col-sm-12';
}

if ( $top_footer_layout == 'footer-5' ) {
	$col_top_footer_class[0] = $col_top_footer_class[1] = $col_top_footer_class[2] = 'col-md-4 col-sm-12';
}

if ( $top_footer_layout == 'footer-6' ) {
	$col_top_footer_class[0] = 'col-md-9 col-sm-12';
	$col_top_footer_class[1] = 'col-md-3 col-sm-12';
}

if ( $top_footer_layout == 'footer-7' ) {
	$col_top_footer_class[0] = 'col-md-3 col-sm-12';
	$col_top_footer_class[1] = 'col-md-9 col-sm-12';
}

if ( $top_footer_layout == 'footer-8' ) {
	$col_top_footer_class[0] = 'col-md-3 col-sm-12';
	$col_top_footer_class[1] = 'col-md-6 col-sm-12';
	$col_top_footer_class[2] = 'col-md-3 col-sm-12';
}

if ( $top_footer_layout == 'footer-9' ) {
	$col_top_footer_class[0] = 'col-md-12 col-sm-12';
}
//End top-footer-layout


$col_footer = 0;
for ( $i = 1; $i <= 4; $i ++ ) {
	if ( is_active_sidebar( 'footer-' . $i ) ) {
		$col_footer += 1;
	}
}
$col_footer_class = array();

if ( $footer_layout == 'footer-1' ) {
	$col_footer_class[0] = $col_footer_class[1] = $col_footer_class[2] = $col_footer_class[3] = 'col-md-3 col-sm-6';
}
if ( $footer_layout == 'footer-2' ) {
	$col_footer_class[0] = 'col-md-6 col-sm-12';
	$col_footer_class[1] = $col_footer_class[2] = 'col-md-3 col-sm-6';
}

if ( $footer_layout == 'footer-3' ) {
	$col_footer_class[0] = $col_footer_class[1] = 'col-md-3 col-sm-6';
	$col_footer_class[2] = 'col-md-6 col-sm-12';
}

if ( $footer_layout == 'footer-4' ) {
	$col_footer_class[0] = $col_footer_class[1] = 'col-md-6 col-sm-12';
}

if ( $footer_layout == 'footer-5' ) {
	$col_footer_class[0] = $col_footer_class[1] = $col_footer_class[2] = 'col-md-4 col-sm-12';
}

if ( $footer_layout == 'footer-6' ) {
	$col_footer_class[0] = 'col-md-9 col-sm-12';
	$col_footer_class[1] = 'col-md-3 col-sm-12';
}

if ( $footer_layout == 'footer-7' ) {
	$col_footer_class[0] = 'col-md-3 col-sm-12';
	$col_footer_class[1] = 'col-md-9 col-sm-12';
}

if ( $footer_layout == 'footer-8' ) {
	$col_footer_class[0] = 'col-md-3 col-sm-12';
	$col_footer_class[1] = 'col-md-6 col-sm-12';
	$col_footer_class[2] = 'col-md-3 col-sm-12';
}

if ( $footer_layout == 'footer-9' ) {
	$col_footer_class[0] = 'col-md-12 col-sm-12';
}
$footer_above_layout = isset( $g5plus_options['footer_above_layout'] ) ? $g5plus_options['footer_above_layout'] : 'col-md-6 col-md-push-3';
?>
<div class="main-footer">
	<div class="footer_inner clearfix">
		<?php if ( $col_top_footer > 0 ): ?>
			<div class="top-footer footer_top_holder col-<?php echo esc_attr( $col_top_footer ); ?>">
				<div class="container">
					<div class="row top-footer-col-<?php echo esc_attr( $col_top_footer . ' ' . $top_footer_layout ); ?>">
						<?php
						for ( $j = 1; $j <= 4; $j ++ ) {
							if ( is_active_sidebar( 'top-footer-' . $j ) ) {
								if ( count( $col_top_footer_class ) >= $j ) {
									echo '<div class="sidebar ' . esc_attr( $col_top_footer_class[$j - 1] ) . ' col-' . $j . '">';
									dynamic_sidebar( 'top-footer-' . $j );
									echo '</div>';
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if ( $col_footer > 0 ): ?>
			<div class="footer_top_holder col-<?php echo esc_attr( $col_footer ); ?>">
				<div class="container">
					<div class="row footer-top-col-<?php echo esc_attr( $col_footer . ' ' . $footer_layout ); ?>">
						<?php
						for ( $j = 1; $j <= 4; $j ++ ) {
							if ( is_active_sidebar( 'footer-' . $j ) ) {
								if ( count( $col_footer_class ) >= $j ) {
									echo '<div class="sidebar ' . esc_attr( $col_footer_class[$j - 1] ) . ' col-' . $j . '">';
									dynamic_sidebar( 'footer-' . $j );
									echo '</div>';
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
<!--Tracking Code-->
<div id="fb-root"></div>
<script>(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=594824607339998";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<script type='text/javascript'>window._sbzq || function (e) {
		e._sbzq = [];
		var t = e._sbzq;
		t.push(["_setAccount", 37199]);
		var n = e.location.protocol == "https:" ? "https:" : "http:";
		var r = document.createElement("script");
		r.type = "text/javascript";
		r.async = true;
		r.src = n + "//static.subiz.com/public/js/loader.js";
		var i = document.getElementsByTagName("script")[0];
		i.parentNode.insertBefore(r, i)
	}(window);</script>


<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-75287869-1', 'auto');
	ga('send', 'pageview');

</script>

<!--End tracking code-->
