<?php
/*---------------------------------------------------
/* COMMENT FIELDS
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_comment_fields' ) ) {
	function g5plus_comment_fields( $fields ) {

		$commenter = wp_get_current_commenter();
		$req       = get_option( 'require_name_email' );
		$aria_req  = ( $req ? " aria-required='true'" : '' );
		$html5     = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';;

		$fields = array(
			'author' => '<div class="form-group col-md-6">' .
				'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="' . esc_html__( 'Name*', 'hemelios' ) . '" ' . $aria_req . '>' .
				'</div>',
			'url'    => '<div class="form-group col-md-6">' .
				'<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="' . esc_html__( 'Website', 'hemelios' ) . '" />' .
				'</div>',
			'email'  => '<div class="form-group col-md-12">' .
				'<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" placeholder="' . esc_html__( 'Email*', 'hemelios' ) . '" ' . $aria_req . '>' .
				'</div>'
		);

		return $fields;

	}

	add_filter( 'g5plus_comment_fields', 'g5plus_comment_fields' );
}

/*---------------------------------------------------
/* COMMENT FORMS ARGS
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_comment_form_args' ) ) {
	function g5plus_comment_form_args( $comment_form_args ) {
		$commenter = wp_get_current_commenter();
		$req       = get_option( 'require_name_email' );
		$aria_req  = ( $req ? " aria-required='true'" : '' );
		$html5     = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';;

		$comment_form_args['comment_field'] = '<div class="form-group col-md-12">' .
			'<textarea rows="6" id="comment" name="comment" placeholder="' . esc_html__( 'Message*', 'hemelios' ) . '" ' . $aria_req . '></textarea>' .
			'</div>';

		$comment_form_args['class_submit'] = 'os-button style1 size-md';
		$comment_form_args['label_submit'] = esc_html__( 'SEND MESSAGE', 'hemelios' );

		return $comment_form_args;
	}

	add_filter( 'g5plus_comment_form_args', 'g5plus_comment_form_args' );
}

/*
 * REARRANGE THE COMMENT FIELDS
 */
if ( !function_exists( 'g5plus_move_comment_field_to_bottom' ) ):
	/**
	 * @param $fields
	 *
	 * @return mixed
	 * @author: @sinzii
	 */
	function g5plus_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;

		return $fields;
	}

	add_filter( 'comment_form_fields', 'g5plus_move_comment_field_to_bottom' );

endif;

///*
// * ADD PHONE COMMENT META PARAM
// */
//
//if ( !function_exists( 'g5plus_add_phone_param' ) ):
//	function g5plus_add_phone_param( $comment_id ) {
//		if ( isset( $_POST['phone'] ) ) {
//			$phone = wp_filter_nohtml_kses( $_POST['phone'] );
//			add_comment_meta( $comment_id, 'phone', $phone, false );
//		}
//	}
//
//	add_action( 'comment_post', 'g5plus_add_phone_param' );
//endif;
//
//if ( !function_exists( 'g5plus_add_phone_column' ) ):
//
//	function add_comments_customs_columns( $columns ) {
//		$new = array();
//		foreach ( $columns as $key => $title ) {
//			if ( $key == 'comment' ) // Put the Thumbnail column before the Author column
//			{
//				$new['phone'] = esc_html( 'Phone Number' );
//			}
//			$new[$key] = $title;
//		}
//
//		return $new;
//	}
//
//	function set_comments_customs_columns_value( $column, $comment_ID ) {
//		$phone = get_comment_meta( $comment_ID, 'phone', true );
//
////		if (empty($phone)) {
////			$phone = '';
////		}
//		if ( $column == 'phone' ) {
//			echo $phone;
//		}
//	}
//
//	function g5plus_add_phone_column() {
//		if ( is_admin() ) {
//			add_filter( 'manage_edit-comments_columns', 'add_comments_customs_columns' );
//			add_filter( 'manage_comments_custom_column', 'set_comments_customs_columns_value', 10, 2 );
//		}
//	}
//
//	g5plus_add_phone_column();
//
//endif;


/*---------------------------------------------------
/* SET ONE PAGE MENU
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_main_menu_one_page_filter' ) ) {
	function g5plus_main_menu_one_page_filter( $args ) {
		if ( isset( $args['theme_location'] ) && ( $args['theme_location'] != 'primary' ) ) {
			return $args;
		}
		$prefix      = 'g5plus_';
		$is_one_page = rwmb_meta( $prefix . 'is_one_page' );
		if ( $is_one_page == '1' ) {
			$args['menu_class'] .= ' menu-one-page';
		}

		return $args;
	}

	add_filter( 'wp_nav_menu_args', 'g5plus_main_menu_one_page_filter', 20 );
}

/*---------------------------------------------------
/* HEADER CUSTOMIZE
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_header_customize_filter' ) ) {
	add_filter( 'g5plus_header_customize_filter', 'g5plus_header_customize_filter' );
	function g5plus_header_customize_filter( $args ) {
		$g5plus_options = g5plus_option();

		$prefix = 'g5plus_';

		$enable_header_customize = rwmb_meta( $prefix . 'enable_header_customize' );

		$header_customize = array();
		if ( $enable_header_customize == '1' ) {
			$page_header_customize = rwmb_meta( $prefix . 'header_customize' );
			if ( isset( $page_header_customize['enable'] ) && !empty( $page_header_customize['enable'] ) ) {
				$header_customize = explode( '||', $page_header_customize['enable'] );
			}
		} else {
			if ( isset( $g5plus_options['header_customize'] ) && isset( $g5plus_options['header_customize']['enabled'] ) && is_array( $g5plus_options['header_customize']['enabled'] ) ) {
				foreach ( $g5plus_options['header_customize']['enabled'] as $key => $value ) {
					$header_customize[] = $key;
				}
			}
		}

		ob_start();
		if ( count( $header_customize ) > 0 ) {
			?>
			<div class="header-customize">
				<?php foreach ( $header_customize as $key ) {
					switch ( $key ) {
						case 'search':
							g5plus_get_template( 'header/search-button' );
							break;
						case 'shopping-cart':
							if ( class_exists( 'WooCommerce' ) ) {
								g5plus_get_template( 'header/mini-cart' );
							}
							break;
						case 'get-a-quote':
							g5plus_get_template( 'header/get-a-quote' );
							break;
					}
				} ?>
			</div>
			<?php
		}

		return ob_get_clean();
	}
}

/*---------------------------------------------------
/* AFTER MAIN MENU (for header 3)
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_after_main_menu_filter' ) ) {
	add_filter( 'g5plus_after_main_menu_filter', 'g5plus_after_main_menu_filter' );
	function g5plus_after_main_menu_filter( $args ) {
		$g5plus_options = g5plus_option();

		$main_menu_after_customize = isset( $g5plus_options['main_menu_after_customize'] ) ?
			$g5plus_options['main_menu_after_customize'] : '';
		ob_start();
		if ( !empty( $main_menu_after_customize ) ):
			$main_menu_after_customize = apply_filters( 'g5plus_do_shortcode', $main_menu_after_customize );
			?>
			<div class="main-menu-custom-text">
				<?php echo wp_kses_post( $main_menu_after_customize ); ?>
			</div>
			<?php
		endif;

		return ob_get_clean();
	}
}

/*---------------------------------------------------
/* ADD SEARCH FORM TO BEFORE X-MENU
/*---------------------------------------------------*/
if ( !function_exists( 'g5plus_search_form_before_xmenu' ) ) {
	function g5plus_search_form_before_xmenu( $params ) {
		ob_start();
		?>
		<li class="menu-fly-search">
			<form method="get" action="<?php echo esc_url( site_url() ); ?>">
				<input type="text" name="s" placeholder="<?php echo esc_html__( 'Search...', 'hemelios' ); ?>">
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
		</li>
		<?php
		$params .= ob_get_clean();

		return $params;
	}

	add_filter( 'xmenu_filter_before', 'g5plus_search_form_before_xmenu', 10 );
}