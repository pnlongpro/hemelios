<?php
/*
*
*	Meta Box Functions
*	------------------------------------------------
*	G5Plus Framework
* 	Copyright Swift Ideas 2015 - http://www.g5plus.net
*
*/
global $meta_boxes;

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function g5plus_register_meta_boxes() {
	global $meta_boxes;
	$prefix = 'g5plus_';
	/* PAGE MENU */
	$menu_list = array();
	if ( function_exists( 'g5plus_get_menu_list' ) ) {
		$menu_list = g5plus_get_menu_list();
	}

// POST FORMAT: Image
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Image', 'hemelios' ),
		'id'     => $prefix . 'meta_box_post_format_image',
		'pages'  => array( 'post' ),
		'format' => 'post-format',
		'fields' => array(
			array(
				'name'             => esc_html__( 'Image', 'hemelios' ),
				'id'               => $prefix . 'post_format_image',
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'desc'             => esc_html__( 'Select a image for post', 'hemelios' )
			),
		),
	);

// POST FORMAT: Gallery
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Gallery', 'hemelios' ),
		'format' => 'post-format',
		'id'     => $prefix . 'meta_box_post_format_gallery',
		'pages'  => array( 'post' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Images', 'hemelios' ),
				'id'   => $prefix . 'post_format_gallery',
				'type' => 'image_advanced',
				'desc' => esc_html__( 'Select images gallery for post', 'hemelios' )
			),
		),
	);

// POST FORMAT: Video
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Video', 'hemelios' ),
		'format' => 'post-format',
		'id'     => $prefix . 'meta_box_post_format_video',
		'pages'  => array( 'post' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Video URL or Embeded Code', 'hemelios' ),
				'id'   => $prefix . 'post_format_video',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: Audio
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Audio', 'hemelios' ),
		'format' => 'post-format',
		'id'     => $prefix . 'meta_box_post_format_audio',
		'pages'  => array( 'post' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Audio URL or Embeded Code', 'hemelios' ),
				'id'   => $prefix . 'post_format_audio',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: QUOTE
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Quote', 'hemelios' ),
		'format' => 'post-format',
		'id'     => $prefix . 'meta_box_post_format_quote',
		'pages'  => array( 'post' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Quote', 'hemelios' ),
				'id'   => $prefix . 'post_format_quote',
				'type' => 'textarea',
			),
			array(
				'name' => esc_html__( 'Author', 'hemelios' ),
				'id'   => $prefix . 'post_format_quote_author',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Author Url', 'hemelios' ),
				'id'   => $prefix . 'post_format_quote_author_url',
				'type' => 'url',
			),
		),
	);
	// POST FORMAT: LINK
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'Post Format: Link', 'hemelios' ),
		'format' => 'post-format',
		'id'     => $prefix . 'meta_box_post_format_link',
		'pages'  => array( 'post' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Url', 'hemelios' ),
				'id'   => $prefix . 'post_format_link_url',
				'type' => 'url',
			),
			array(
				'name' => esc_html__( 'Text', 'hemelios' ),
				'id'   => $prefix . 'post_format_link_text',
				'type' => 'text',
			),
		),
	);
//GALLERY
	$meta_boxes[] = array(
		'id'     => $prefix . 'serives_gallery_meta_box',
		'title'  => esc_html__( 'Gallery', 'hemelios' ),
		'pages'  => array( 'services' ),
		'fields' => array(
			array(
				'name' => esc_html__( 'Images', 'hemelios' ),
				'id'   => $prefix . 'services_gallery',
				'type' => 'image_advanced',
				'desc' => esc_html__( 'Select images gallery for post', 'hemelios' )
			),
		)
	);
// PAGE TITLE
//--------------------------------------------------
	$meta_boxes[] = array(
		'id'     => $prefix . 'page_title_meta_box',
		'title'  => esc_html__( 'Page Title', 'hemelios' ),
		'pages'  => array( 'post', 'page', 'portfolio', 'services', 'ourteam', 'product' ),
		'fields' => array(
			array(
				'name'    => esc_html__( 'Show/Hide Page Title?', 'hemelios' ),
				'id'      => $prefix . 'show_page_title',
				'type'    => 'button_set',
				'std'     => '-1',
				'options' => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show Page Title', 'hemelios' ),
					'0'  => esc_html__( 'Hide Page Title', 'hemelios' ),
				)
			),

			// PAGE TITLE LINE 1
			array(
				'name'           => esc_html__( 'Custom Page Title', 'hemelios' ),
				'id'             => $prefix . 'page_title_custom',
				'desc'           => esc_html__( "Enter a custom page title if you'd like.", 'hemelios' ),
				'type'           => 'text',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Page Description', 'hemelios' ),
				'id'             => $prefix . 'page_description',
				'desc'           => esc_html__( "Enter a page description if you'd like.", 'hemelios' ),
				'type'           => 'textarea',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),
			// PAGE TITLE TEXT COLOR
			array(
				'name'           => esc_html__( 'Page Title Text Color', 'hemelios' ),
				'id'             => $prefix . 'page_title_text_color',
				'desc'           => esc_html__( "Optionally set a text color for the page title.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),

			// PAGE TITLE BACKGROUND COLOR
			array(
				'name'           => esc_html__( 'Page Title Background Color', 'hemelios' ),
				'id'             => $prefix . 'page_title_bg_color',
				'desc'           => esc_html__( "Optionally set a background color for the page title.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),

			// BACKGROUND IMAGE
			array(
				'id'               => $prefix . 'page_title_bg_image',
				'name'             => esc_html__( 'Background Image', 'hemelios' ),
				'desc'             => esc_html__( 'Background Image for page title.', 'hemelios' ),
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'required-field'   => array( $prefix . 'show_page_title', '<>', '0' ),
			),

			// PAGE TITLE OVERLAY COLOR
			array(
				'id'             => $prefix . 'page_title_overlay_color',
				'name'           => esc_html__( 'Page Title Overlay Color', 'hemelios' ),
				'desc'           => esc_html__( "Set an overlay color for page title image.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Custom Overlay Opacity?', 'hemelios' ),
				'id'             => $prefix . 'enable_custom_overlay_opacity',
				'type'           => 'checkbox',
				'std'            => 0,
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),


			// Overlay Opacity Value
			array(
				'name'           => esc_html__( 'Overlay Opacity', 'hemelios' ),
				'id'             => $prefix . 'page_title_overlay_opacity',
				'desc'           => esc_html__( 'Set the opacity level of the overlay. This will lighten or darken the image depening on the color selected.', 'hemelios' ),
				'clone'          => false,
				'type'           => 'slider',
				'prefix'         => '',
				'js_options'     => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array( $prefix . 'enable_custom_overlay_opacity', '=', '1' ),
			),

			// PAGE TITLE Height
			array(
				'name'           => esc_html__( 'Page Title Height', 'hemelios' ),
				'id'             => $prefix . 'page_title_height',
				'desc'           => esc_html__( "Enter a page title height value (not include unit).", 'hemelios' ),
				'type'           => 'number',
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),
			// Breadcrumbs in Page Title
			array(
				'name'           => esc_html__( 'Breadcrumbs in Page Title', 'hemelios' ),
				'id'             => $prefix . 'breadcrumbs_in_page_title',
				'desc'           => esc_html__( "Show/Hide Breadcrumbs in Page Title", 'hemelios' ),
				'type'           => 'button_set',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show Breadcrumbs', 'hemelios' ),
					'0'  => esc_html__( 'Hide Breadcrumbs', 'hemelios' ),
				),
				'std'            => '-1',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Breadcrumbs Align', 'hemelios' ),
				'id'             => $prefix . 'breadcrumbs_position',
				'desc'           => esc_html__( "Breadcrumbs Align", 'hemelios' ),
				'type'           => 'button_set',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'0'  => esc_html__( 'Left', 'hemelios' ),
					'1'  => esc_html__( 'Center', 'hemelios' ),
					'2'  => esc_html__( 'Right', 'hemelios' ),
				),
				'std'            => '-1',
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Remove Margin Bottom', 'hemelios' ),
				'id'             => $prefix . 'page_title_remove_margin_bottom',
				'type'           => 'checkbox',
				'std'            => 0,
				'required-field' => array( $prefix . 'show_page_title', '<>', '0' )
			),
		)
	);

// PAGE HEADER
//--------------------------------------------------
	$meta_boxes[] = array(
		'id'     => $prefix . 'page_header_meta_box',
		'title'  => esc_html__( 'Page Header', 'hemelios' ),
		'pages'  => array( 'post', 'page', 'portfolio', 'services', 'ourteam', 'product' ),
		'fields' => array(
			array(
				'name'        => esc_html__( 'Page Menu', 'hemelios' ),
				'id'          => $prefix . 'page_menu',
				'type'        => 'select_advanced',
				'options'     => $menu_list,
				'placeholder' => esc_html__( 'Select Menu', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
				'desc'        => esc_html__( 'Optionally you can choose to override the menu that is used on the page', 'hemelios' ),
			),

			array(
				'name' => esc_html__( 'Is One Page', 'hemelios' ),
				'id'   => $prefix . 'is_one_page',
				'type' => 'checkbox',
				'std'  => 0,
				'desc' => esc_html__( 'Set page style is One Page', 'hemelios' ),
			),

			array(
				'name'        => esc_html__( 'Above Header Sidebar', 'hemelios' ),
				'id'          => $prefix . 'above_header_sidebar',
				'type'        => 'sidebars',
				'placeholder' => esc_html__( 'Select Sidebar', 'hemelios' ),
				'std'         => ''
			),

			array(
				'name'    => esc_html__( 'Show/Hide Top Bar', 'hemelios' ),
				'id'      => $prefix . 'top_bar',
				'type'    => 'button_set',
				'std'     => '-1',
				'options' => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show Top Bar', 'hemelios' ),
					'0'  => esc_html__( 'Hide Top Bar', 'hemelios' )
				),
				'desc'    => esc_html__( 'Show Hide Top Bar.', 'hemelios' ),
			),

			array(
				'name'           => esc_html__( 'Top Bar Layout', 'hemelios' ),
				'id'             => $prefix . 'top_bar_layout',
				'type'           => 'image_set',
				'allowClear'     => true,
				'width'          => '80px',
				'std'            => '',
				'options'        => array(
					'top-bar-1' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-1.jpg',
					'top-bar-2' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-2.jpg',
					'top-bar-3' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-3.jpg',
					'top-bar-4' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-4.jpg',
				),
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Top Left Sidebar', 'hemelios' ),
				'id'             => $prefix . 'top_left_sidebar',
				'type'           => 'sidebars',
				'std'            => '',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Top Right Sidebar', 'hemelios' ),
				'id'             => $prefix . 'top_right_sidebar',
				'type'           => 'sidebars',
				'std'            => '',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Top Bar Background Color', 'hemelios' ),
				'id'             => $prefix . 'topbar_bg_color',
				'desc'           => esc_html__( "Optionally set a background color for the Top bar.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Top Bar Color', 'hemelios' ),
				'id'             => $prefix . 'topbar_color',
				'desc'           => esc_html__( "Optionally set a color for the Top bar.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),
			//opacity header
			array(
				'name'           => esc_html__( 'Show/Hide Top Bar Border', 'hemelios' ),
				'id'             => $prefix . 'show_top_bar_border',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'top_bar', '<>', '0' ),
			),


			array(
				'name'           => esc_html__( 'Top Bar Border Color', 'hemelios' ),
				'id'             => $prefix . 'topbar_border_color',
				'desc'           => esc_html__( "Optionally set a border color for the Top bar.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'show_top_bar_border', '=', '1' ),
			),
			array(
				'name'    => esc_html__( 'Show/Hide Header', 'hemelios' ),
				'id'      => $prefix . 'header_show_hide',
				'type'    => 'button_set',
				'std'     => '1',
				'options' => array(
					'1' => esc_html__( 'Show Header', 'hemelios' ),
					'0' => esc_html__( 'Hide Header', 'hemelios' )
				),
				'desc'    => esc_html__( 'Show/hide header', 'hemelios' ),
			),
			array(
				'name'           => esc_html__( 'Header Layout Style', 'hemelios' ),
				'id'             => $prefix . 'header_layout_style',
				'type'           => 'button_set',
				'allowClear'     => true,
				'std'            => '-1',
				'options'        => array(
					'-1'    => esc_html__( 'Default', 'hemelios' ),
					'boxed' => esc_html__( 'Boxed', 'hemelios' ),
					'wide'  => esc_html__( 'Wide', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Header Layout', 'hemelios' ),
				'id'             => $prefix . 'header_layout',
				'type'           => 'image_set',
				'allowClear'     => true,
				'std'            => '',
				'options'        => array(
					'header-1' => get_template_directory_uri() . '/assets/images/theme-options/header-1.jpg',
					'header-2' => get_template_directory_uri() . '/assets/images/theme-options/header-2.jpg',
					'header-3' => get_template_directory_uri() . '/assets/images/theme-options/header-3.jpg',
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Header Position', 'hemelios' ),
				'id'             => $prefix . 'header_positon',
				'type'           => 'button_set',
				'allowClear'     => true,
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Enable Header Overlay', 'hemelios' ),
					'0'  => esc_html__( 'Disable Header Overlay', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Header Margin Top', 'hemelios' ),
				'id'             => $prefix . 'header_margin_top',
				'desc'           => esc_html__( "Optionally set Margintop for Header.", 'hemelios' ),
				'type'           => 'number',
				'std'            => '',
				'default'        => '0',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Header Background Color', 'hemelios' ),
				'id'             => $prefix . 'header_bg_color',
				'desc'           => esc_html__( "Optionally set a background color for the Header.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),
			//opacity header
			array(
				'name'           => esc_html__( 'Custom Overlay Opacity?', 'hemelios' ),
				'id'             => $prefix . 'header_custom_overlay_opacity',
				'type'           => 'checkbox',
				'std'            => 0,
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			// Overlay Opacity Value
			array(
				'name'           => esc_html__( 'Header Background Overlay Opacity', 'hemelios' ),
				'id'             => $prefix . 'header_bg_opacity',
				'desc'           => esc_html__( 'Set the opacity level of the overlay. This will lighten or darken the image depening on the color selected.', 'hemelios' ),
				'clone'          => false,
				'type'           => 'slider',
				'prefix'         => '',
				'js_options'     => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array( $prefix . 'header_custom_overlay_opacity', '=', '1' ),
			),
			array(
				'name'           => esc_html__( 'Show Border', 'hemelios' ),
				'id'             => $prefix . 'header_show_border',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Header Border Color', 'hemelios' ),
				'id'             => $prefix . 'header_border_color',
				'desc'           => esc_html__( "Optionally set a border color for the Header.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'header_show_border', '=', '1' ),
			),
			// Overlay Opacity Value
			array(
				'name'           => esc_html__( 'Header Border Opacity', 'hemelios' ),
				'id'             => $prefix . 'header_border_opacity',
				'desc'           => esc_html__( 'Set the opacity level of the overlay. This will lighten or darken the image depening on the color selected.', 'hemelios' ),
				'clone'          => false,
				'type'           => 'slider',
				'prefix'         => '',
				'js_options'     => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array( $prefix . 'header_show_border', '=', '1' ),
			),

			// Show cart subtotal
			array(
				'name'           => esc_html__( 'Cart Subtotal', 'hemelios' ),
				'id'             => $prefix . 'cart_subtotal',
				'desc'           => esc_html__( 'Show or hide Cart Subtotal', 'hemelios' ),
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Custom Cart Quantity Icon Background', 'hemelios' ),
				'id'             => $prefix . 'custom_cart_quantity_icon_bg',
				'desc'           => esc_html__( "Using default value (clear the value field) if you want to use the value in Theme Options", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'default'        => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Menu Background Color', 'hemelios' ),
				'id'             => $prefix . 'menu_bg_color',
				'desc'           => esc_html__( "Optionally set a background color for the Menu.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'default'        => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Menu Text Color', 'hemelios' ),
				'id'             => $prefix . 'menu_text_color',
				'desc'           => esc_html__( "Optionally set a text color for the Menu.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'default'        => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Menu Text Hover Color', 'hemelios' ),
				'id'             => $prefix . 'menu_text_hover_color',
				'desc'           => esc_html__( "Optionally set a text hover color for the menu.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'default'        => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),


			array(
				'name'           => esc_html__( 'Set Header Customize?', 'hemelios' ),
				'id'             => $prefix . 'enable_header_customize',
				'type'           => 'checkbox',
				'std'            => 0,
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),
			array(
				'name'           => esc_html__( 'Header Customize', 'hemelios' ),
				'id'             => $prefix . 'header_customize',
				'type'           => 'sorter',
				'std'            => '',
				'desc'           => esc_html__( 'Select element for header customize. Drag to change element order', 'hemelios' ),
				'options'        => array(
					'get-a-quote'   => 'Get a quote',
					'shopping-cart' => 'Shopping Cart',
					'search'        => 'Search Box',
				),
				'required-field' => array( $prefix . 'enable_header_customize', '=', '1' ),
			),

			array(
				'name'           => esc_html__( 'Header Sticky', 'hemelios' ),
				'id'             => $prefix . 'header_sticky',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Enable Header Sticky', 'hemelios' ),
					'0'  => esc_html__( 'Disable Header Sticky', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Header Sticky Background Color', 'hemelios' ),
				'id'             => $prefix . 'header_sticky_bg_color',
				'desc'           => esc_html__( "Custom header sticky background color.", 'hemelios' ),
				'type'           => 'color',
				'std'            => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Header Sticky Border Bottom', 'hemelios' ),
				'id'             => $prefix . 'header_sticky_border',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show Border', 'hemelios' ),
					'0'  => esc_html__( 'Hide Border', 'hemelios' ),
				),
				'required-field' => array( $prefix . 'header_sticky', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Mobile Header Search Box', 'hemelios' ),
				'id'             => $prefix . 'mobile_header_search_box',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' )
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Mobile Header Shopping Cart', 'hemelios' ),
				'id'             => $prefix . 'mobile_header_shopping_cart',
				'type'           => 'button_set',
				'std'            => '-1',
				'options'        => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' )
				),
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'id'               => $prefix . 'custom_logo',
				'name'             => esc_html__( 'Custom Logo', 'hemelios' ),
				'desc'             => esc_html__( 'Upload custom logo in header.', 'hemelios' ),
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'required-field'   => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

			array(
				'id'             => $prefix . 'custom_logo_padding',
				'name'           => esc_html__( 'Custom Logo Padding Top/Bottom', 'hemelios' ),
				'desc'           => esc_html__( 'Set a custom logo padding top and bottom. Just leave blank if you want to use the value in Theme Options.', 'hemelios' ),
				'type'           => 'number',
				'std'            => '',
				'required-field' => array( $prefix . 'header_show_hide', '<>', '0' ),
			),

		)
	);


// PAGE FOOTER
//--------------------------------------------------
	$meta_boxes[] = array(
		'id'     => $prefix . 'page_footer_meta_box',
		'title'  => esc_html__( 'Page Footer', 'hemelios' ),
		'pages'  => array( 'post', 'page', 'portfolio', 'services', 'ourteam', 'product' ),
		'fields' => array(
			array(
				'name'    => esc_html__( 'Show/Hide Footer', 'hemelios' ),
				'id'      => $prefix . 'footer_show_hide',
				'type'    => 'button_set',
				'std'     => '1',
				'options' => array(
					'1' => esc_html__( 'Show Footer', 'hemelios' ),
					'0' => esc_html__( 'Hide Footer', 'hemelios' )
				),
				'desc'    => esc_html__( 'Show/hide footer', 'hemelios' ),
			),

			array(
				'name'       => esc_html__( 'Footer Layout', 'hemelios' ),
				'id'         => $prefix . 'footer_layout',
				'type'       => 'image_set',
				'allowClear' => true,
				'width'      => '80px',
				'std'        => '',
				'options'    => array(
					'footer-1' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-1.jpg',
					'footer-2' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-2.jpg',
					'footer-3' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-3.jpg',
					'footer-4' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-4.jpg',
					'footer-5' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-5.jpg',
					'footer-6' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-6.jpg',
					'footer-7' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-7.jpg',
					'footer-8' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-8.jpg',
					'footer-9' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-9.jpg',
				),
				'desc'       => esc_html__( 'Select Footer Layout (Not set to default).', 'hemelios' ),
			),

			array(
				'name'    => esc_html__( 'Show/Hide Bottom Bar', 'hemelios' ),
				'id'      => $prefix . 'bottom_bar',
				'type'    => 'button_set',
				'std'     => '-1',
				'options' => array(
					'-1' => 'Default',
					'1'  => 'Show Bottom Bar',
					'0'  => 'Hide Bottom Bar'
				),
				'desc'    => esc_html__( 'Show Hide Bottom Bar.', 'hemelios' ),
			),

			array(
				'name'           => esc_html__( 'Bottom Bar Layout', 'hemelios' ),
				'id'             => $prefix . 'bottom_bar_layout',
				'type'           => 'image_set',
				'allowClear'     => true,
				'width'          => '80px',
				'std'            => '',
				'options'        => array(
					'bottom-bar-1' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-1.jpg',
					'bottom-bar-2' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-2.jpg',
					'bottom-bar-3' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-3.jpg',
					'bottom-bar-4' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-4.jpg',
				),
				'desc'           => esc_html__( 'Bottom bar layout.', 'hemelios' ),
				'required-field' => array( $prefix . 'bottom_bar', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Bottom Bar Left Sidebar', 'hemelios' ),
				'id'             => $prefix . 'bottom_bar_left_sidebar',
				'type'           => 'sidebars',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'std'            => '',
				'required-field' => array( $prefix . 'bottom_bar', '<>', '0' ),
			),

			array(
				'name'           => esc_html__( 'Bottom Bar Right Sidebar', 'hemelios' ),
				'id'             => $prefix . 'bottom_bar_right_sidebar',
				'type'           => 'sidebars',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'std'            => '',
				'required-field' => array( $prefix . 'bottom_bar', '<>', '0' ),
			),

		)
	);

// PAGE LAYOUT
	$meta_boxes[] = array(
		'id'     => $prefix . 'page_layout_meta_box',
		'title'  => esc_html__( 'Page Layout', 'hemelios' ),
		'pages'  => array( 'post', 'page', 'services', 'ourteam', 'product' ),
		'fields' => array(
			array(
				'name'     => esc_html__( 'Layout Style', 'hemelios' ),
				'id'       => $prefix . 'layout_style',
				'type'     => 'button_set',
				'options'  => array(
					'-1'    => esc_html__( 'Default', 'hemelios' ),
					'boxed' => esc_html__( 'Boxed', 'hemelios' ),
					'wide'  => esc_html__( 'Wide', 'hemelios' )
				),
				'std'      => '-1',
				'multiple' => false,
			),
			array(
				'name'     => esc_html__( 'Page Layout', 'hemelios' ),
				'id'       => $prefix . 'page_layout',
				'type'     => 'button_set',
				'options'  => array(
					'-1'              => esc_html__( 'Default', 'hemelios' ),
					'full'            => esc_html__( 'Full Width', 'hemelios' ),
					'container'       => esc_html__( 'Container', 'hemelios' ),
					'container-fluid' => esc_html__( 'Container Fluid', 'hemelios' ),
				),
				'std'      => '-1',
				'multiple' => false,
			),
			array(
				'name'       => esc_html__( 'Page Sidebar', 'hemelios' ),
				'id'         => $prefix . 'page_sidebar',
				'type'       => 'image_set',
				'allowClear' => true,
				'options'    => array(
					'none'  => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png',
					'left'  => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png',
					'right' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png',
					'both'  => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png'
				),
				'std'        => '',
				'multiple'   => false,

			),
			array(
				'name'           => esc_html__( 'Left Sidebar', 'hemelios' ),
				'id'             => $prefix . 'page_left_sidebar',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'type'           => 'sidebars',
				'std'            => '',
				'required-field' => array( $prefix . 'page_sidebar', '=', array( '', 'left', 'both' ) ),
			),

			array(
				'name'           => esc_html__( 'Right Sidebar', 'hemelios' ),
				'id'             => $prefix . 'page_right_sidebar',
				'type'           => 'sidebars',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'std'            => '',
				'required-field' => array( $prefix . 'page_sidebar', '=', array( '', 'right', 'both' ) ),
			),

			array(
				'name'           => esc_html__( 'Sidebar Width', 'hemelios' ),
				'id'             => $prefix . 'sidebar_width',
				'type'           => 'button_set',
				'options'        => array(
					'-1'     => esc_html__( 'Default', 'hemelios' ),
					'small'  => esc_html__( 'Small (1/4)', 'hemelios' ),
					'larger' => esc_html__( 'Large (1/3)', 'hemelios' )
				),
				'std'            => '-1',
				'multiple'       => false,
				'required-field' => array( $prefix . 'page_sidebar', '<>', 'none' ),
			),


			array(
				'name'    => esc_html__( 'Custom Bottom Page Sidebar?', 'hemelios' ),
				'id'      => $prefix . 'show_page_bottom_sidebar',
				'type'           => 'button_set',
				'options' => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' )
				),
				'desc'    => esc_html__( 'Choose default mean use Theme Options value.','hemelios' ),
				'std'     => '-1',
			),

			array(
				'name'           => esc_html__( 'Bottom Sidebar', 'hemelios' ),
				'id'             => $prefix . 'page_bottom_sidebar',
				'type'           => 'sidebars',
				'placeholder'    => esc_html__( 'Select Sidebar', 'hemelios' ),
				'desc'           => esc_html__( 'This will override the value in Theme Options', 'hemelios' ),
				'std'            => '',
				'required-field' => array( $prefix . 'show_page_bottom_sidebar', '=', '1' ),
			),

			array(
				'name' => esc_html__( 'Page Class Extra', 'hemelios' ),
				'id'   => $prefix . 'page_class_extra',
				'type' => 'text',
				'std'  => ''
			),
			array(
				'name'    => esc_html__( 'Page Margin Bottom', 'hemelios' ),
				'id'      => $prefix . 'page_content_mb',
				'type'    => 'button_set',
				'desc'    => esc_html__( 'Choose default mean use Theme Options value.','hemelios' ),
				'options' => array(
					'-1' => esc_html__( 'Default', 'hemelios' ),
					'1'  => esc_html__( 'Show', 'hemelios' ),
					'0'  => esc_html__( 'Hide', 'hemelios' )
				),
				'std'     => '-1',
			),
		)
	);

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}

// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'g5plus_register_meta_boxes' );
