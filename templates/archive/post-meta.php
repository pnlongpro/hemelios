<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/9/2015
 * Time: 8:55 AM
 */
?>
<ul class="entry-meta">
	<li class="entry-meta-author">
		<?php printf( '<a href="%1$s">Posted by <span itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name">%2$s</span></span></a>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), esc_html( get_the_author() ) ); ?>
	</li>
	<li class="entry-meta-date">
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"> <span itemprop="datePublished"><?php echo human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) . ' trước'; ?></span> </a>
	</li>
	<?php if ( has_category() ): ?>
		<li class="entry-meta-category">
			<span itemprop="articleSection"><?php echo esc_html__( 'Posted in ', 'hemelios' ) ?><?php echo get_the_category_list( ', ' ); ?></span>
		</li>
	<?php endif; ?>
	<?php edit_post_link( esc_html__( 'Edit', 'hemelios' ), '<li class="edit-link">', '</li>' ); ?>
</ul>
