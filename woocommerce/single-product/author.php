<?php
/**
 * Single Product Author
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/7/2016
 * Time: 10:36 AM
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product_author = get_post_meta( get_the_ID(), 'g5plus_product_author', true );
?>

<?php

if ( !empty( $product_author ) ): ?>
	<div class="product-author">by <span><?php echo esc_html($product_author); ?></span> </div>
<?php endif; ?>
