<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */


$archive_title_bg_url = get_template_directory_uri() . '/assets/images/bg-archive-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Archive Options', 'hemelios' ),
		'desc'       => '',
		'subsection' => true,
		'icon'       => 'el el-th-large',
		'fields'     => array(
			array(
				'id'       => 'archive_blog_style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Blog Style', 'hemelios' ),
				'subtitle' => esc_html__( 'Select Blog Style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'style-1' => 'style 1', 'style-2' => 'style-2' ),
				'default'  => 'style-2'
			),
			array(
				'id'       => 'archive_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select archive layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),

			array(
				'id'       => 'archive_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'right'
			),


			array(
				'id'       => 'archive_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'archive_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),

			array(
				'id'       => 'archive_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-1',
				'required' => array( 'archive_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'archive_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-1',
				'required' => array( 'archive_sidebar', '=', array( 'right', 'both' ) ),
			),
			array(
				'id'       => 'archive_paging_style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Paging Style', 'hemelios' ),
				'subtitle' => esc_html__( 'Select archive paging style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'default' => 'Default', 'load-more' => 'Load More', 'infinity-scroll' => 'Infinity Scroll' ),
				'default'  => 'default'
			),

			array(
				'id'       => 'show_archive_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Archive Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable archive title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'archive_title_default',
				'type'     => 'text',
				'title'    => esc_html__( 'Title Default', 'hemelios' ),
				'subtitle' => esc_html__( 'Archive Title Default', 'hemelios' ),
				'desc'     => '',
				'required' => array( 'show_archive_title', '=', array( '1' ) ),
				'default'  => 'News'
			),
			array(
				'id'       => 'archive_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Archive Title Height', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the archive title here.', 'hemelios' ),
				'required' => array( 'show_archive_title', '=', array( '1' ) ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '420'
				)
			),

			array(
				'id'       => 'breadcrumbs_in_archive_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in archive title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'required' => array( 'show_archive_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'archive_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Archive Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select archive breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_archive_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'archive_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Archive Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload archive title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $archive_title_bg_url
				)
			),

		)
	) );