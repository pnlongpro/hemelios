<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

$portfolio_title_bg_url    = get_template_directory_uri() . '/assets/images/bg-portfolio-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Portfolio', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-dashboard',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'show_portfolio_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Portfolio Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable portfolio title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'portfolio_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Portfolio Title Height', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px) or empty.', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the portfolio title here.', 'hemelios' ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '420'
				)
			),
			array(
				'id'       => 'breadcrumbs_in_portfolio_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Portfolio Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in portfolio title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),

			array(
				'id'       => 'portfolio_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Portfolio Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select portfolio breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_portfolio_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'portfolio_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Portfolio Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload portfolio title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $portfolio_title_bg_url
				)
			),
			array(
					'id'    => 'portfolio_info',
					'type'     => 'button_set',
					'title'    => esc_html__( 'Show Portfolio Info ', 'hemelios' ),
					'subtitle' => esc_html__( 'Enable/Disable Portfolio Info', 'hemelios' ),
					'desc'     => '',
					'options'  => array( '1' => 'Show', '0' => 'Hiden' ),
					'default'  => '1'
			),
		)
	) );