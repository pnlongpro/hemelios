<?php
/**
 * Created by PhpStorm.
 * User: duonglh
 * Date: 8/23/14
 * Time: 3:01 PM
 */

function g5plus_generate_less() {
	try {
		//global $g5plus_options;
		$g5plus_options = get_option( 'g5plus_hemelios_options' );
		if ( !defined( 'FS_METHOD' ) ) {
			define( 'FS_METHOD', 'direct' );
		}

		$regex = array(
			"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism" => "\n"
		);

		$home_preloader = $g5plus_options['home_preloader'];
		$css_variable   = g5plus_custom_css_variable();
		$custom_css     = g5plus_custom_css();

		$fileout = get_template_directory() . "/assets/css/less/config.less";
		if ( !file_put_contents( $fileout, $css_variable, LOCK_EX ) ) {
			@chmod( $fileout, 0777 );
			file_put_contents( $fileout, $css_variable, LOCK_EX );
		}

		if ( !class_exists( 'Less_Parser' ) ) {
			require_once get_template_directory() . '/g5plus-framework/less/Autoloader.php';
			Less_Autoloader::register();
		}

		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		WP_Filesystem();
		global $wp_filesystem;

		// DEFINE OPTIONS
		$less_option  = array( 'compress' => false );
		$style_suffix = '';
		$theme_info   = $wp_filesystem->get_contents(  get_template_directory()  . "/theme-info.txt" );
		// CHANGE OPTIONS IF COMPRESS FILE IS ENABLE
		if ( isset( $g5plus_options['enable_minifile_css'] ) && $g5plus_options['enable_minifile_css'] == '1' ) {
			$less_option  = array( 'compress' => true );
			$style_suffix = '.min';
			$theme_info   = '';
		}

		//
		$parser = new Less_Parser( $less_option );

		$parser->parse( $css_variable );
		$parser->parseFile(  get_template_directory()  . '/assets/css/less/style.less' );



		if ( $home_preloader != 'none' && !empty( $home_preloader ) ) {
			$parser->parseFile(  get_template_directory()  . '/assets/css/less/loading/' . $home_preloader . '.less' );
		}

		if ( isset( $g5plus_options['panel_selector'] ) && ( $g5plus_options['panel_selector'] == '1' ) ) {
			$parser->parseFile(  get_template_directory()  . '/assets/css/less/panel-style-selector.less' );
		}

		$parser->parse( $custom_css );

		$css = $parser->getCss();

		// APPEND THEME INFO
		if ( !empty( $theme_info ) ) {
			$css = $theme_info . "\n" . $css;
		}

		$css = preg_replace( array_keys( $regex ), $regex, $css );

		// PUT CONTENT
		if ( !$wp_filesystem->put_contents(  get_template_directory()  . '/style' . $style_suffix . '.css', $css, FS_CHMOD_FILE ) ) {
			return array(
				'status'  => 'error',
				'message' => esc_html__( 'Could not save file', 'hemelios' )
			);
		}

		return array(
			'status'  => 'success',
			'message' => ''
		);

	} catch ( Exception $e ) {
		$error_message = $e->getMessage();

		return array(
			'status'  => 'error',
			'message' => $error_message
		);
	}
}