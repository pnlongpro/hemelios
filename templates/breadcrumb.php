<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/3/2015
 * Time: 10:20 AM
 */
$g5plus_options = g5plus_option();
$prefix                      = 'g5plus_';
$g5plus_breadcrumbs_position = g5plus_get_post_meta_box_option( $prefix . 'breadcrumbs_position' );

if ( ( $g5plus_breadcrumbs_position === '' ) || ( $g5plus_breadcrumbs_position == '-1' ) ) {

	if ( is_singular() ) {
		switch ( get_post_type() ) {
			case 'page':
				$g5plus_breadcrumbs_position = $g5plus_options['page_breadcrumbs_position'];
				break;
			case 'product':
				$g5plus_breadcrumbs_position = $g5plus_options['single_product_breadcrumbs_position'];
				break;
			case 'portfolio':
				$g5plus_breadcrumbs_position = $g5plus_options['portfolio_breadcrumbs_position'];
				break;
			case 'post':
				$g5plus_breadcrumbs_position = $g5plus_options['single_blog_breadcrumbs_position'];
				break;
			case 'services':
				$g5plus_breadcrumbs_position = $g5plus_options['service_breadcrumbs_position'];
				break;
			case 'ourteam':
				$g5plus_breadcrumbs_position = $g5plus_options['ourteam_breadcrumbs_position'];
				break;
			default:
				$g5plus_breadcrumbs_position = 'not match post type';
		}
	}


	if ( is_archive() || is_search() || is_front_page() ) {
		$g5plus_breadcrumbs_position = $g5plus_options['archive_breadcrumbs_position'];

		if ( get_post_type() == 'product' ) {
			$g5plus_breadcrumbs_position = $g5plus_options['archive_product_breadcrumbs_position'];
		} else {
			if ( get_post_type() == 'portfolio' ) {
				$g5plus_breadcrumbs_position = $g5plus_options['portfolio_breadcrumbs_position'];
			} else {
				if ( get_post_type() == 'services' ) {
					$g5plus_breadcrumbs_position = $g5plus_options['service_breadcrumbs_position'];
				} else {
					if ( get_post_type() == 'ourteam' ) {
						$g5plus_breadcrumbs_position = $g5plus_options['ourteam_breadcrumbs_position'];
					}
				}
			}
		}
	}

}

global $class_breadcrumbs;
switch ( $g5plus_breadcrumbs_position ) {
	case '0':
		$class_breadcrumbs = 'breadcrumbs-left';
		break;
	case '1':
		$class_breadcrumbs = 'breadcrumbs-center';
		break;
	case '2':
		$class_breadcrumbs = 'breadcrumbs-right';
		break;
	default:
		$class_breadcrumbs = 'breadcrumbs-left';
}
?>
<?php if ( !is_front_page() ) : ?>
	<?php g5plus_get_breadcrumb(); ?>
<?php else: ?>
	<ul class="breadcrumbs <?php echo esc_attr( $class_breadcrumbs ); ?>">
		<li class="first" typeof="v:Breadcrumb"><?php echo esc_html__( 'You are here: ', 'hemelios' ) ?></li>
		<li class="home">
			<a rel="v:url" href="<?php echo esc_html( home_url( '/' ) ) ?>" class="home"><?php echo esc_html__( 'Trang chủ', 'hemelios' ) ?></a>
		</li>
		<li><span><?php echo esc_html__( 'Blog', 'hemelios' ); ?></span></li>
	</ul>
<?php endif; ?>

