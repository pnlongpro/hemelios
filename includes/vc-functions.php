<?php

add_action( 'vc_before_init', 'g5plus_vcSetAsTheme' );
function g5plus_vcSetAsTheme() {
	vc_set_as_theme();
}

// Add custome icon font
function vc_iconpicker_type_hemelios( $icons ) {
	$hemelios_icons = array(
		array( 'icon-adjustments' => 'Adjustments' ),
		array( 'icon-alarmclock' => 'Alarmclock' ),
		array( 'icon-anchor' => 'Anchor' ),
		array( 'icon-aperture' => 'Aperture' ),
		array( 'icon-attachments' => 'Attachments' ),
		array( 'icon-back28' => 'Back28' ),
		array( 'icon-bargraph' => 'Bargraph' ),
		array( 'icon-basket' => 'Basket' ),
		array( 'icon-beaker' => 'Beaker' ),
		array( 'icon-bike' => 'Bike' ),
		array( 'icon-book-open' => 'Book Open' ),
		array( 'icon-briefcase' => 'Briefcase' ),
		array( 'icon-browser' => 'Browser' ),
		array( 'icon-calendar' => 'Calendar' ),
		array( 'icon-camera' => 'Camera' ),
		array( 'icon-caution' => 'Caution' ),
		array( 'icon-chat' => 'Chat' ),
		array( 'icon-circle-compass' => 'Circle Compass' ),
		array( 'icon-clipboard' => 'Clipboard' ),
		array( 'icon-clock' => 'Clock' ),
		array( 'icon-cloud' => 'Cloud' ),
		array( 'icon-compass' => 'Compass' ),
		array( 'icon-desktop' => 'Desktop' ),
		array( 'icon-dial' => 'Dial' ),
		array( 'icon-document' => 'Document' ),
		array( 'icon-documents' => 'Documents' ),
		array( 'icon-downarrows10' => 'Downarrows10' ),
		array( 'icon-download' => 'Download' ),
		array( 'icon-dribbble' => 'Dribbble' ),
		array( 'icon-edit' => 'Edit' ),
		array( 'icon-envelope' => 'Envelope' ),
		array( 'icon-expand' => 'Expand' ),
		array( 'icon-facebook' => 'Facebook' ),
		array( 'icon-flag' => 'Flag' ),
		array( 'icon-focus' => 'Focus' ),
		array( 'icon-gears' => 'Gears' ),
		array( 'icon-genius' => 'Genius' ),
		array( 'icon-gift' => 'Gift' ),
		array( 'icon-global' => 'Global' ),
		array( 'icon-globe' => 'Globe' ),
		array( 'icon-googleplus' => 'Googleplus' ),
		array( 'icon-grid' => 'Grid' ),
		array( 'icon-happy' => 'Happy' ),
		array( 'icon-hazardous' => 'Hazardous' ),
		array( 'icon-heart' => 'Heart' ),
		array( 'icon-hotairballoon' => 'Hotairballoon' ),
		array( 'icon-hourglass' => 'Hourglass' ),
		array( 'icon-key' => 'Key' ),
		array( 'icon-laptop' => 'Laptop' ),
		array( 'icon-layers' => 'Layers' ),
		array( 'icon-leftarrow59' => 'Leftarrow59' ),
		array( 'icon-lifesaver' => 'Lifesaver' ),
		array( 'icon-lightbulb' => 'Lightbulb' ),
		array( 'icon-linegraph' => 'Linegraph' ),
		array( 'icon-linkedin' => 'Linkedin' ),
		array( 'icon-lock' => 'Lock' ),
		array( 'icon-magnifying-glass' => 'Magnifying Glass' ),
		array( 'icon-magnifying-glass34' => 'Magnifying Glass34' ),
		array( 'icon-map' => 'Map' ),
		array( 'icon-map-pin' => 'Map Pin' ),
		array( 'icon-megaphone' => 'Megaphone' ),
		array( 'icon-mic' => 'Mic' ),
		array( 'icon-mobile' => 'Mobile' ),
		array( 'icon-mouse61-1' => 'Mouse61 1' ),
		array( 'icon-newspaper' => 'Newspaper' ),
		array( 'icon-notebook' => 'Notebook' ),
		array( 'icon-paintbrush' => 'Paintbrush' ),
		array( 'icon-paperclip' => 'Paperclip' ),
		array( 'icon-pencil' => 'Pencil' ),
		array( 'icon-phone' => 'Phone' ),
		array( 'icon-picture' => 'Picture' ),
		array( 'icon-pictures' => 'Pictures' ),
		array( 'icon-piechart' => 'Piechart' ),
		array( 'icon-presentation' => 'Presentation' ),
		array( 'icon-pricetags' => 'Pricetags' ),
		array( 'icon-printer' => 'Printer' ),
		array( 'icon-profile-female' => 'Profile Female' ),
		array( 'icon-profile-male' => 'Profile Male' ),
		array( 'icon-puzzle' => 'Puzzle' ),
		array( 'icon-quote' => 'Quote' ),
		array( 'icon-recycle' => 'Recycle' ),
		array( 'icon-refresh' => 'Refresh' ),
		array( 'icon-ribbon' => 'Ribbon' ),
		array( 'icon-right127' => 'Right127' ),
		array( 'icon-rightarrow59' => 'Rightarrow59' ),
		array( 'icon-rss' => 'Rss' ),
		array( 'icon-sad' => 'Sad' ),
		array( 'icon-scissors' => 'Scissors' ),
		array( 'icon-scope' => 'Scope' ),
		array( 'icon-search' => 'Search' ),
		array( 'icon-shield' => 'Shield' ),
		array( 'icon-shopping111' => 'Shopping111' ),
		array( 'icon-speedometer' => 'Speedometer' ),
		array( 'icon-strategy' => 'Strategy' ),
		array( 'icon-streetsign' => 'Streetsign' ),
		array( 'icon-tablet' => 'Tablet' ),
		array( 'icon-telescope' => 'Telescope' ),
		array( 'icon-toolbox' => 'Toolbox' ),
		array( 'icon-tools' => 'Tools' ),
		array( 'icon-tools-2' => 'Tools 2' ),
		array( 'icon-traget' => 'Traget' ),
		array( 'icon-trophy' => 'Trophy' ),
		array( 'icon-tumblr' => 'Tumblr' ),
		array( 'icon-twitter' => 'Twitter' ),
		array( 'icon-uparrows15' => 'Uparrows15' ),
		array( 'icon-upload' => 'Upload' ),
		array( 'icon-video' => 'Video' ),
		array( 'icon-wallet' => 'Wallet' ),
		array( 'icon-warning45' => 'Warning45' ),
		array( 'icon-wine' => 'Wine' ),
		array( 'icon-shopper29' => 'Shopper29' ),
		array( 'icon-doc2' => 'Doc2' ),
		array( 'icon-download8' => 'Download8' ),
		array( 'icon-pdf19' => 'Pdf19' ),
		array( 'icon-ppt' => 'Ppt' ),
		array( 'icon-address' => 'Address' ),
		array( 'icon-adjust' => 'Adjust' ),
		array( 'icon-air' => 'Air' ),
		array( 'icon-alert' => 'Alert' ),
		array( 'icon-archive' => 'Archive' ),
		array( 'icon-arrow-combo' => 'Arrow Combo' ),
		array( 'icon-arrows-ccw' => 'Arrows Ccw' ),
		array( 'icon-attach' => 'Attach' ),
		array( 'icon-attention' => 'Attention' ),
		array( 'icon-back' => 'Back' ),
		array( 'icon-back-in-time' => 'Back In Time' ),
		array( 'icon-bag' => 'Bag' ),
		array( 'icon-basket-1' => 'Basket 1' ),
		array( 'icon-battery' => 'Battery' ),
		array( 'icon-behance' => 'Behance' ),
		array( 'icon-bell' => 'Bell' ),
		array( 'icon-block' => 'Block' ),
		array( 'icon-book' => 'Book' ),
		array( 'icon-book-open-1' => 'Book Open 1' ),
		array( 'icon-bookmark' => 'Bookmark' ),
		array( 'icon-bookmarks' => 'Bookmarks' ),
		array( 'icon-box' => 'Box' ),
		array( 'icon-briefcase-1' => 'Briefcase 1' ),
		array( 'icon-brush' => 'Brush' ),
		array( 'icon-bucket' => 'Bucket' ),
		array( 'icon-calendar-1' => 'Calendar 1' ),
		array( 'icon-camera-1' => 'Camera 1' ),
		array( 'icon-cancel' => 'Cancel' ),
		array( 'icon-cancel-circled' => 'Cancel Circled' ),
		array( 'icon-cancel-squared' => 'Cancel Squared' ),
		array( 'icon-cc' => 'Cc' ),
		array( 'icon-cc-by' => 'Cc By' ),
		array( 'icon-cc-nc' => 'Cc Nc' ),
		array( 'icon-cc-nc-eu' => 'Cc Nc Eu' ),
		array( 'icon-cc-nc-jp' => 'Cc Nc Jp' ),
		array( 'icon-cc-nd' => 'Cc Nd' ),
		array( 'icon-cc-pd' => 'Cc Pd' ),
		array( 'icon-cc-remix' => 'Cc Remix' ),
		array( 'icon-cc-sa' => 'Cc Sa' ),
		array( 'icon-cc-share' => 'Cc Share' ),
		array( 'icon-cc-zero' => 'Cc Zero' ),
		array( 'icon-ccw' => 'Ccw' ),
		array( 'icon-cd' => 'Cd' ),
		array( 'icon-chart-area' => 'Chart Area' ),
		array( 'icon-chart-bar' => 'Chart Bar' ),
		array( 'icon-chart-line' => 'Chart Line' ),
		array( 'icon-chart-pie' => 'Chart Pie' ),
		array( 'icon-chat-1' => 'Chat 1' ),
		array( 'icon-check' => 'Check' ),
		array( 'icon-clipboard-1' => 'Clipboard 1' ),
		array( 'icon-clock-1' => 'Clock 1' ),
		array( 'icon-cloud-1' => 'Cloud 1' ),
		array( 'icon-cloud-thunder' => 'Cloud Thunder' ),
		array( 'icon-code' => 'Code' ),
		array( 'icon-cog' => 'Cog' ),
		array( 'icon-comment' => 'Comment' ),
		array( 'icon-compass-1' => 'Compass 1' ),
		array( 'icon-credit-card' => 'Credit Card' ),
		array( 'icon-cup' => 'Cup' ),
		array( 'icon-cw' => 'Cw' ),
		array( 'icon-database' => 'Database' ),
		array( 'icon-db-shape' => 'Db Shape' ),
		array( 'icon-direction' => 'Direction' ),
		array( 'icon-doc' => 'Doc' ),
		array( 'icon-doc-landscape' => 'Doc Landscape' ),
		array( 'icon-doc-text' => 'Doc Text' ),
		array( 'icon-doc-text-inv' => 'Doc Text Inv' ),
		array( 'icon-docs' => 'Docs' ),
		array( 'icon-dot' => 'Dot' ),
		array( 'icon-dot-2' => 'Dot 2' ),
		array( 'icon-dot-3' => 'Dot 3' ),
		array( 'icon-down' => 'Down' ),
		array( 'icon-down-bold' => 'Down Bold' ),
		array( 'icon-down-circled' => 'Down Circled' ),
		array( 'icon-down-dir' => 'Down Dir' ),
		array( 'icon-down-open' => 'Down Open' ),
		array( 'icon-down-open-big' => 'Down Open Big' ),
		array( 'icon-down-open-mini' => 'Down Open Mini' ),
		array( 'icon-down-thin' => 'Down Thin' ),
		array( 'icon-download-1' => 'Download 1' ),
		array( 'icon-dribbble-1' => 'Dribbble 1' ),
		array( 'icon-dribbble-circled' => 'Dribbble Circled' ),
		array( 'icon-drive' => 'Drive' ),
		array( 'icon-dropbox' => 'Dropbox' ),
		array( 'icon-droplet' => 'Droplet' ),
		array( 'icon-erase' => 'Erase' ),
		array( 'icon-evernote' => 'Evernote' ),
		array( 'icon-export' => 'Export' ),
		array( 'icon-eye' => 'Eye' ),
		array( 'icon-facebook-1' => 'Facebook 1' ),
		array( 'icon-facebook-circled' => 'Facebook Circled' ),
		array( 'icon-facebook-squared' => 'Facebook Squared' ),
		array( 'icon-fast-backward' => 'Fast Backward' ),
		array( 'icon-fast-forward' => 'Fast Forward' ),
		array( 'icon-feather' => 'Feather' ),
		array( 'icon-flag-1' => 'Flag 1' ),
		array( 'icon-flash' => 'Flash' ),
		array( 'icon-flashlight' => 'Flashlight' ),
		array( 'icon-flattr' => 'Flattr' ),
		array( 'icon-flickr' => 'Flickr' ),
		array( 'icon-flickr-circled' => 'Flickr Circled' ),
		array( 'icon-flight' => 'Flight' ),
		array( 'icon-floppy' => 'Floppy' ),
		array( 'icon-flow-branch' => 'Flow Branch' ),
		array( 'icon-flow-cascade' => 'Flow Cascade' ),
		array( 'icon-flow-line' => 'Flow Line' ),
		array( 'icon-flow-parallel' => 'Flow Parallel' ),
		array( 'icon-flow-tree' => 'Flow Tree' ),
		array( 'icon-folder' => 'Folder' ),
		array( 'icon-forward' => 'Forward' ),
		array( 'icon-gauge' => 'Gauge' ),
		array( 'icon-github' => 'Github' ),
		array( 'icon-github-circled' => 'Github Circled' ),
		array( 'icon-globe-1' => 'Globe 1' ),
		array( 'icon-google-circles' => 'Google Circles' ),
		array( 'icon-gplus' => 'Gplus' ),
		array( 'icon-gplus-circled' => 'Gplus Circled' ),
		array( 'icon-graduation-cap' => 'Graduation Cap' ),
		array( 'icon-heart-1' => 'Heart 1' ),
		array( 'icon-heart-empty' => 'Heart Empty' ),
		array( 'icon-help' => 'Help' ),
		array( 'icon-help-circled' => 'Help Circled' ),
		array( 'icon-home' => 'Home' ),
		array( 'icon-hourglass-1' => 'Hourglass 1' ),
		array( 'icon-inbox' => 'Inbox' ),
		array( 'icon-infinity' => 'Infinity' ),
		array( 'icon-info' => 'Info' ),
		array( 'icon-info-circled' => 'Info Circled' ),
		array( 'icon-instagrem' => 'Instagrem' ),
		array( 'icon-install' => 'Install' ),
		array( 'icon-key-1' => 'Key 1' ),
		array( 'icon-keyboard' => 'Keyboard' ),
		array( 'icon-lamp' => 'Lamp' ),
		array( 'icon-language' => 'Language' ),
		array( 'icon-lastfm' => 'Lastfm' ),
		array( 'icon-lastfm-circled' => 'Lastfm Circled' ),
		array( 'icon-layout' => 'Layout' ),
		array( 'icon-leaf' => 'Leaf' ),
		array( 'icon-left' => 'Left' ),
		array( 'icon-left-bold' => 'Left Bold' ),
		array( 'icon-left-circled' => 'Left Circled' ),
		array( 'icon-left-dir' => 'Left Dir' ),
		array( 'icon-left-open' => 'Left Open' ),
		array( 'icon-left-open-big' => 'Left Open Big' ),
		array( 'icon-left-open-mini' => 'Left Open Mini' ),
		array( 'icon-left-thin' => 'Left Thin' ),
		array( 'icon-level-down' => 'Level Down' ),
		array( 'icon-level-up' => 'Level Up' ),
		array( 'icon-lifebuoy' => 'Lifebuoy' ),
		array( 'icon-light-down' => 'Light Down' ),
		array( 'icon-light-up' => 'Light Up' ),
		array( 'icon-link' => 'Link' ),
		array( 'icon-linkedin-1' => 'Linkedin 1' ),
		array( 'icon-linkedin-circled' => 'Linkedin Circled' ),
		array( 'icon-list' => 'List' ),
		array( 'icon-list-add' => 'List Add' ),
		array( 'icon-location' => 'Location' ),
		array( 'icon-lock-1' => 'Lock 1' ),
		array( 'icon-lock-open' => 'Lock Open' ),
		array( 'icon-login' => 'Login' ),
		array( 'icon-logo-db' => 'Logo Db' ),
		array( 'icon-logout' => 'Logout' ),
		array( 'icon-loop' => 'Loop' ),
		array( 'icon-magnet' => 'Magnet' ),
		array( 'icon-mail' => 'Mail' ),
		array( 'icon-map-1' => 'Map 1' ),
		array( 'icon-megaphone-1' => 'Megaphone 1' ),
		array( 'icon-menu' => 'Menu' ),
		array( 'icon-mic-1' => 'Mic 1' ),
		array( 'icon-minus' => 'Minus' ),
		array( 'icon-minus-circled' => 'Minus Circled' ),
		array( 'icon-minus-squared' => 'Minus Squared' ),
		array( 'icon-mixi' => 'Mixi' ),
		array( 'icon-mobile-1' => 'Mobile 1' ),
		array( 'icon-monitor' => 'Monitor' ),
		array( 'icon-moon' => 'Moon' ),
		array( 'icon-mouse' => 'Mouse' ),
		array( 'icon-music' => 'Music' ),
		array( 'icon-mute' => 'Mute' ),
		array( 'icon-network' => 'Network' ),
		array( 'icon-newspaper-1' => 'Newspaper 1' ),
		array( 'icon-note' => 'Note' ),
		array( 'icon-note-beamed' => 'Note Beamed' ),
		array( 'icon-palette' => 'Palette' ),
		array( 'icon-paper-plane' => 'Paper Plane' ),
		array( 'icon-pause' => 'Pause' ),
		array( 'icon-paypal' => 'Paypal' ),
		array( 'icon-pencil-1' => 'Pencil 1' ),
		array( 'icon-phone-1' => 'Phone 1' ),
		array( 'icon-picasa' => 'Picasa' ),
		array( 'icon-picture-1' => 'Picture 1' ),
		array( 'icon-pinterest' => 'Pinterest' ),
		array( 'icon-pinterest-circled' => 'Pinterest Circled' ),
		array( 'icon-play' => 'Play' ),
		array( 'icon-plus' => 'Plus' ),
		array( 'icon-plus-circled' => 'Plus Circled' ),
		array( 'icon-plus-squared' => 'Plus Squared' ),
		array( 'icon-popup' => 'Popup' ),
		array( 'icon-print' => 'Print' ),
		array( 'icon-progress-0' => 'Progress 0' ),
		array( 'icon-progress-1' => 'Progress 1' ),
		array( 'icon-progress-2' => 'Progress 2' ),
		array( 'icon-progress-3' => 'Progress 3' ),
		array( 'icon-publish' => 'Publish' ),
		array( 'icon-qq' => 'Qq' ),
		array( 'icon-quote-1' => 'Quote 1' ),
		array( 'icon-rdio' => 'Rdio' ),
		array( 'icon-rdio-circled' => 'Rdio Circled' ),
		array( 'icon-record' => 'Record' ),
		array( 'icon-renren' => 'Renren' ),
		array( 'icon-reply' => 'Reply' ),
		array( 'icon-reply-all' => 'Reply All' ),
		array( 'icon-resize-full' => 'Resize Full' ),
		array( 'icon-resize-small' => 'Resize Small' ),
		array( 'icon-retweet' => 'Retweet' ),
		array( 'icon-right' => 'Right' ),
		array( 'icon-right-bold' => 'Right Bold' ),
		array( 'icon-right-circled' => 'Right Circled' ),
		array( 'icon-right-dir' => 'Right Dir' ),
		array( 'icon-right-open' => 'Right Open' ),
		array( 'icon-right-open-big' => 'Right Open Big' ),
		array( 'icon-right-open-mini' => 'Right Open Mini' ),
		array( 'icon-right-thin' => 'Right Thin' ),
		array( 'icon-rocket' => 'Rocket' ),
		array( 'icon-rss-1' => 'Rss 1' ),
		array( 'icon-search-1' => 'Search 1' ),
		array( 'icon-share' => 'Share' ),
		array( 'icon-shareable' => 'Shareable' ),
		array( 'icon-shuffle' => 'Shuffle' ),
		array( 'icon-signal' => 'Signal' ),
		array( 'icon-sina-weibo' => 'Sina Weibo' ),
		array( 'icon-skype' => 'Skype' ),
		array( 'icon-skype-circled' => 'Skype Circled' ),
		array( 'icon-smashing' => 'Smashing' ),
		array( 'icon-sound' => 'Sound' ),
		array( 'icon-soundcloud' => 'Soundcloud' ),
		array( 'icon-spotify' => 'Spotify' ),
		array( 'icon-spotify-circled' => 'Spotify Circled' ),
		array( 'icon-star' => 'Star' ),
		array( 'icon-star-empty' => 'Star Empty' ),
		array( 'icon-stop' => 'Stop' ),
		array( 'icon-stumbleupon' => 'Stumbleupon' ),
		array( 'icon-stumbleupon-circled' => 'Stumbleupon Circled' ),
		array( 'icon-suitcase' => 'Suitcase' ),
		array( 'icon-sweden' => 'Sweden' ),
		array( 'icon-switch' => 'Switch' ),
		array( 'icon-tag' => 'Tag' ),
		array( 'icon-tape' => 'Tape' ),
		array( 'icon-target' => 'Target' ),
		array( 'icon-thermometer' => 'Thermometer' ),
		array( 'icon-thumbs-down' => 'Thumbs Down' ),
		array( 'icon-thumbs-up' => 'Thumbs Up' ),
		array( 'icon-ticket' => 'Ticket' ),
		array( 'icon-to-end' => 'To End' ),
		array( 'icon-to-start' => 'To Start' ),
		array( 'icon-tools-1' => 'Tools 1' ),
		array( 'icon-traffic-cone' => 'Traffic Cone' ),
		array( 'icon-trash' => 'Trash' ),
		array( 'icon-trophy-1' => 'Trophy 1' ),
		array( 'icon-tumblr-1' => 'Tumblr 1' ),
		array( 'icon-tumblr-circled' => 'Tumblr Circled' ),
		array( 'icon-twitter-1' => 'Twitter 1' ),
		array( 'icon-twitter-circled' => 'Twitter Circled' ),
		array( 'icon-up' => 'Up' ),
		array( 'icon-up-bold' => 'Up Bold' ),
		array( 'icon-up-circled' => 'Up Circled' ),
		array( 'icon-up-dir' => 'Up Dir' ),
		array( 'icon-up-open' => 'Up Open' ),
		array( 'icon-up-open-big' => 'Up Open Big' ),
		array( 'icon-up-open-mini' => 'Up Open Mini' ),
		array( 'icon-up-thin' => 'Up Thin' ),
		array( 'icon-upload-1' => 'Upload 1' ),
		array( 'icon-upload-cloud' => 'Upload Cloud' ),
		array( 'icon-user' => 'User' ),
		array( 'icon-user-add' => 'User Add' ),
		array( 'icon-users' => 'Users' ),
		array( 'icon-vcard' => 'Vcard' ),
		array( 'icon-video-1' => 'Video 1' ),
		array( 'icon-vimeo' => 'Vimeo' ),
		array( 'icon-vimeo-circled' => 'Vimeo Circled' ),
		array( 'icon-vkontakte' => 'Vkontakte' ),
		array( 'icon-volume' => 'Volume' ),
		array( 'icon-water' => 'Water' ),
		array( 'icon-window' => 'Window' ),
	);

	return array_merge( $icons, $hemelios_icons );
}

function vc_icon_hemelios_font_enqueue() {
	wp_enqueue_style( 'hemelios-icon' );
}

add_filter( 'vc_iconpicker-type-hemelios', 'vc_iconpicker_type_hemelios' );
add_action( 'vc_backend_editor_enqueue_js_css', 'vc_icon_hemelios_font_enqueue' );
add_action( 'vc_frontend_editor_enqueue_js_css', 'vc_icon_hemelios_font_enqueue' );

// Add function to update single param
/**
 * Update , Add visual composer's shortcodes param
 *
 * @param $shortcode
 * @param $key
 * @param $value
 */
function g5plus_vc_update_single_param_value( $shortcode, $param_name, $key, $value ) {

	$param_group                = WPBMap::getParam( $shortcode, $param_name );
	$param_group['value'][$key] = $value;

	vc_update_shortcode_param( $shortcode, $param_group );
}

function g5plus_vc_remove_frontend_links() {
	vc_disable_frontend();
}

add_action( 'vc_after_init', 'g5plus_vc_remove_frontend_links' );

function g5plus_number_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );
	$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
	$type       = isset( $settings['type'] ) ? $settings['type'] : '';
	$min        = isset( $settings['min'] ) ? $settings['min'] : '';
	$max        = isset( $settings['max'] ) ? $settings['max'] : '';
	$suffix     = isset( $settings['suffix'] ) ? $settings['suffix'] : '';
	$class      = isset( $settings['class'] ) ? $settings['class'] : '';
	$output     = '<input type="number" min="' . esc_attr( $min ) . '" max="' . esc_attr( $max ) . '" class="wpb_vc_param_value ' . esc_attr( $param_name ) . ' ' . esc_attr( $type ) . ' ' . esc_attr( $class ) . '" name="' . esc_attr( $param_name ) . '" value="' . esc_attr( $value ) . '" style="max-width:100px; margin-right: 10px;" />' . esc_attr( $suffix );

	return $output;
}

function g5plus_icon_text_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );

	return '<div class="vc-text-icon">'
	. '<span class="icon-preview" style="height:34px"><i style="width:auto" class="' . esc_attr( $value ) . '"></i></span>'
	. '<input  name="' . $settings['param_name'] . '" style="display:none" class="wpb_vc_param_value wpb-textinput widefat input-icon ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" type="text" value="' . esc_attr( $value ) . '" ' . $dependency . '/>'
	. '<input title="' . esc_html__( 'Click to browse icon', 'hemelios' ) . '" style="height:34px; width:auto" class="browse-icon button-secondary" type="button" value="' . esc_html__( 'Browse Icon', 'hemelios' ) . '" >'
	. '</div>';
}

function g5plus_multi_select_settings_field_shortcode_param( $settings, $value ) {
	$param_name   = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
	$param_option = isset( $settings['options'] ) ? $settings['options'] : '';
	$dependency   = vc_generate_dependencies_attributes( $settings );
	$output       = '<input type="hidden" name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . '" value="' . $value . '"  ' . $dependency . ' />';
	$output .= '<select multiple id="' . $param_name . '_select2" name="' . $param_name . '_select2" class="multi-select">';
	if ( $param_option != '' && is_array( $param_option ) ) {
		foreach ( $param_option as $text_val => $val ) {
			if ( is_numeric( $text_val ) && ( is_string( $val ) || is_numeric( $val ) ) ) {
				$text_val = $val;
			}
			$output .= '<option id="' . $val . '" value="' . $val . '">' . htmlspecialchars( $text_val ) . '</option>';
		}
	}

	$output .= '</select><input type="checkbox" id="' . $param_name . '_select_all" >' . esc_html__( 'Select All', 'hemelios' );
	$output .= '<script type="text/javascript">
		        jQuery(document).ready(function($){

					$("#' . $param_name . '_select2").select2();

					var order = $("#' . $param_name . '").val()
					if (order != "") {
						order = order.split(",");
						var choices = [];
						for (var i = 0; i < order.length; i++) {
							var option = $("#' . $param_name . '_select2 option[value="+ order[i]  + "]");
							choices[i] = {id:order[i], text:option[0].label, element: option};
						}
						$("#' . $param_name . '_select2").select2("data", choices);

					}

			        $("#' . $param_name . '_select2").on("select2-selecting", function(e) {
			            var ids = $("#' . $param_name . '").val();
			            if (ids != "") {
			                ids +=",";
			            }
			            ids += e.val;
			            $("#' . $param_name . '").val(ids);
                    }).on("select2-removed", function(e) {
				          var ids = $("#' . $param_name . '").val();
				          var arr_ids = ids.split(",");
				          var newIds = "";
				          for(var i = 0 ; i < arr_ids.length; i++) {
				            if (arr_ids[i] != e.val){
				                if (newIds != "") {
			                        newIds +=",";
					            }
					            newIds += arr_ids[i];
				            }
				          }
				          $("#' . $param_name . '").val(newIds);
		             });

		            $("#' . $param_name . '_select_all").click(function(){
		                if($("#' . $param_name . '_select_all").is(":checked") ){
		                    $("#' . $param_name . '_select2 > option").prop("selected","selected");
		                    $("#' . $param_name . '_select2").trigger("change");
		                    var arr_ids =  $("#' . $param_name . '_select2").select2("val");
		                    var ids = "";
                            for (var i = 0; i < arr_ids.length; i++ ) {
                                if (ids != "") {
                                    ids +=",";
                                }
                                ids += arr_ids[i];
                            }
                            $("#' . $param_name . '").val(ids);

		                }else{
		                    $("#' . $param_name . '_select2 > option").removeAttr("selected");
		                    $("#' . $param_name . '_select2").trigger("change");
		                    $("#' . $param_name . '").val("");
		                }
		            });
		        });
		        </script>
		        <style>
		            .multi-select
		            {
		              width: 100%;
		            }
		            .select2-drop
		            {
		                z-index: 100000;
		            }
		        </style>';

	return $output;
}

function g5plus_tags_settings_field_shortcode_param( $settings, $value ) {
	$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
	$dependency = vc_generate_dependencies_attributes( $settings );
	$output     = '<input  name="' . $settings['param_name']
		. '" class="wpb_vc_param_value wpb-textinput '
		. $settings['param_name'] . ' ' . $settings['type']
		. '" type="hidden" value="' . $value . '"/>';
	$output .= '<input type="text" name="' . $param_name . '_tagsinput" id="' . $param_name . '_tagsinput" value="' . $value . '" data-role="tagsinput" ' . $dependency . ' />';
	$output .= '<script type="text/javascript">
							jQuery(document).ready(function($){
								$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();

								$("#' . $param_name . '_tagsinput").on("itemAdded", function(event) {
		                             $("input[name=' . $param_name . ']").val($(this).val());
								});

								$("#' . $param_name . '_tagsinput").on("itemRemoved", function(event) {
		                             $("input[name=' . $param_name . ']").val($(this).val());
								});
							});
						</script>';

	return $output;
}

function os_attachment_settings_field( $settings, $value ) {
	$input_value = "";
	if ( $value != "" ) {
		$values = explode( ',', $value );
		foreach ( $values as $val ) {
			$link = vc_parse_multi_attribute( $val, array( 'url' => '', 'title' => '', 'ext' => '' ) );
			$input_value .= "<a href='" . $link['url'] . "' >" . $link['title'] . "</a>";
		}
	}

	return '<div class="os_attachment_block">'
	. var_dump( $values )
	. '<input type="text" style="display:none" id="os_download_input" value="' . $input_value . '"/>'
	. '<input type="text" style="display:none" id="os_download_input_result" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' .
	esc_attr( $settings['param_name'] ) . ' ' .
	esc_attr( $settings['type'] ) . '_field" type="text" value="' . esc_attr( $value ) . '"
                     />'
	. '<div>File: <b id="os_download_input_preview"></b></div><br/>'
	. '<button type="button" id="insert-media-button" class="button insert-media add_media" data-editor="os_download_input">' . __( 'Add Files', 'hemelios' ) . '</button>'
	. '<button type="button" class="button" id="button_remove_all_file">' . __( 'Remove All', 'hemelios' ) . '</button>'
	. '</div>'
	. '<script>(function () {
    "use strict";
    var parseHTML = function (str) {
        var tmp = document.implementation.createHTMLDocument();
        tmp.body.innerHTML = str;
        return tmp.body.children;
    };
    var old_value = "";
    var input_area = document.getElementById("os_download_input");
    var input_result = document.getElementById("os_download_input_result");
    var preview_area = document.getElementById("os_download_input_preview");
    var remove_button = document.getElementById("button_remove_all_file");

    function updateAll(current_value) {
        var input = parseHTML(current_value);
        var result = "";
        var preview = "";
        var first = 1;
        var validate = true;
        if(current_value === ""){
            validate  = true;
        }
        for (var i = 0; i < input.length; i++) {
            if(input[i].querySelectorAll(\'img\').length < 1) {

                var url = input[i].getAttribute("href");
                var title = input[i].textContent;
                var ext = "";
                ext = url.split(\'.\').pop();
                if(ext.length <=5 ){
                    if (first-- <= 0) {
                        result += ",";
                        preview += ", ";
                        first = false;
                    }
                    result += "url:" + encodeURIComponent(url)
                        + "|title:" + encodeURIComponent(title)
                        + "|ext:" + encodeURIComponent(ext);

                    preview += input[i].textContent + \'.\' + ext;
                }else {
                    validate = false;
                }
            }
        }
        if(validate){
            input_result.value = result;
            preview_area.innerHTML = preview;
        }else {
            input_result.value="";
            preview_area.innerHTML = "<span style=\'color:red\'>(Remove all and change link to of files were used to media file in add files window)</span>"
        }
    }

    if (input_area) {
        remove_button.addEventListener("click", function () {
            input_area.value = "";
        }, false);

        setInterval(function () {
            var current_value = input_area.value;
            if (current_value != old_value) {
                updateAll(current_value);
                old_value = current_value;
            }
        }, 200);
    }
})();</script>';

}
if (function_exists('vc_add_' . 'shortcode_param')) {
	call_user_func('vc_add_' . 'shortcode_param', 'number', 'g5plus_number_settings_field');
	call_user_func('vc_add_' . 'shortcode_param', 'icon_text', 'g5plus_icon_text_settings_field');
	call_user_func('vc_add_' . 'shortcode_param', 'multi-select', 'g5plus_multi_select_settings_field_shortcode_param');
	call_user_func('vc_add_' . 'shortcode_param', 'tags', 'g5plus_tags_settings_field_shortcode_param');
	call_user_func('vc_add_' . 'shortcode_param', 'os_attachment', 'os_attachment_settings_field');
}

function g5plus_get_css_animation( $css_animation ) {
	$output = '';
	if ( $css_animation != '' ) {
		wp_enqueue_script( 'waypoints' );
		$output = ' wpb_animate_when_almost_visible g5plus-css-animation ' . $css_animation;
	}

	return $output;
}

function g5plus_get_style_animation( $duration, $delay ) {
	$styles = array();
	if ( $duration != '0' && !empty( $duration ) ) {
		$duration = (float) trim( $duration, "\n\ts" );
		$styles[] = "-webkit-animation-duration: {$duration}s";
		$styles[] = "-moz-animation-duration: {$duration}s";
		$styles[] = "-ms-animation-duration: {$duration}s";
		$styles[] = "-o-animation-duration: {$duration}s";
		$styles[] = "animation-duration: {$duration}s";
	}
	if ( $delay != '0' && !empty( $delay ) ) {
		$delay    = (float) trim( $delay, "\n\ts" );
		$styles[] = "opacity: 0";
		$styles[] = "-webkit-animation-delay: {$delay}s";
		$styles[] = "-moz-animation-delay: {$delay}s";
		$styles[] = "-ms-animation-delay: {$delay}s";
		$styles[] = "-o-animation-delay: {$delay}s";
		$styles[] = "animation-delay: {$delay}s";
	}
	if ( count( $styles ) > 1 ) {
		return 'style="' . implode( ';', $styles ) . '"';
	}

	return implode( ';', $styles );
}

function g5plus_convert_hex_to_rgba( $hex, $opacity = 1 ) {
	$hex = str_replace( "#", "", $hex );
	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );
	}
	$rgba = 'rgba(' . $r . ',' . $g . ',' . $b . ',' . $opacity . ')';

	return $rgba;
}


// Accordion Style
vc_add_param( 'vc_tta_accordion', array(
		'type'        => 'dropdown',
		'param_name'  => 'style',
		'value'       => array(
			esc_html__( 'Hemelios', 'hemelios' )   => 'accordion_style1',
			esc_html__( 'Hemelios 2', 'hemelios' ) => 'accordion_style2',
			esc_html__( 'Classic', 'hemelios' )    => 'classic',
			esc_html__( 'Modern', 'hemelios' )     => 'modern',
			esc_html__( 'Flat', 'hemelios' )       => 'flat',
			esc_html__( 'Outline', 'hemelios' )    => 'outline',
		),
		'heading'     => esc_html__( 'Style', 'hemelios' ),
		'description' => esc_html__( 'Select accordion display style.', 'hemelios' ),
		'weight'      => 1,
	)
);
vc_add_param( 'vc_tta_section', array(
	'type'        => 'iconpicker',
	'heading'     => __( 'Icon', 'js_composer' ),
	'param_name'  => 'i_icon_hemelios',
	'value'       => 'icon icon-layers',
	'settings'    => array(
		'emptyIcon'    => false, // default true, display an "EMPTY" icon?
		'type'         => 'hemelios',
		'iconsPerPage' => 1000, // default 100, how many icons per/page to display
	),
	'dependency'  => array(
		'element' => 'i_type',
		'value'   => 'hemelios',
	),
	'description' => __( 'Select icon from library.', 'js_composer' ),
) );

vc_add_param( 'vc_tta_section', array(
	'type'        => 'textfield',
	'heading'     => esc_html__( 'Extra class name', 'hemelios' ),
	'param_name'  => 'el_class',
	'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.
Close', 'hemelios' ),
	'weight'      => - 1
) );

// Tabs Style
vc_add_param( 'vc_tta_tabs', array(
		'type'        => 'dropdown',
		'param_name'  => 'style',
		'value'       => array(
			esc_html__( 'Hemelios Tab Style 1', 'js_composer' ) => 'tab_style1',
			esc_html__( 'Hemelios Tab Style 2', 'js_composer' ) => 'tab_style2',
			esc_html__( 'Hemelios Tab Style 3', 'js_composer' ) => 'tab_style3',
			esc_html__( 'Hemelios Tab Style 4', 'js_composer' ) => 'tab_style4',
		),
		'heading'     => esc_html__( 'Style', 'js_composer' ),
		'description' => esc_html__( 'Select tabs display style.', 'js_composer' ),
		'weight'      => 1,
	)
);
$settings_vc_map = array(
	'category' => esc_html__( 'Hemelios Shortcodes', 'hemelios' )
);
// Add custom icon to icon picker

vc_map_update( 'vc_tta_tabs', $settings_vc_map );
vc_map_update( 'vc_tta_accordion', $settings_vc_map );
vc_map_update( 'vc_progress_bar', $settings_vc_map );

function register_vc_map() {
	// Add appear animation
	$add_css_animation = array(
		'type'        => 'dropdown',
		'heading'     => esc_html__( 'CSS Animation', 'hemelios' ),
		'param_name'  => 'css_animation',
		'value'       => array(
			esc_html__( 'No', 'hemelios' )                   => '',
			esc_html__( 'Fade In', 'hemelios' )              => 'wpb_fadeIn',
			esc_html__( 'Fade Top to Bottom', 'hemelios' )   => 'wpb_fadeInDown',
			esc_html__( 'Fade Bottom to Top', 'hemelios' )   => 'wpb_fadeInUp',
			esc_html__( 'Fade Left to Right', 'hemelios' )   => 'wpb_fadeInLeft',
			esc_html__( 'Fade Right to Left', 'hemelios' )   => 'wpb_fadeInRight',
			esc_html__( 'Bounce In', 'hemelios' )            => 'wpb_bounceIn',
			esc_html__( 'Bounce Top to Bottom', 'hemelios' ) => 'wpb_bounceInDown',
			esc_html__( 'Bounce Bottom to Top', 'hemelios' ) => 'wpb_bounceInUp',
			esc_html__( 'Bounce Left to Right', 'hemelios' ) => 'wpb_bounceInLeft',
			esc_html__( 'Bounce Right to Left', 'hemelios' ) => 'wpb_bounceInRight',
			esc_html__( 'Zoom In', 'hemelios' )              => 'wpb_zoomIn',
			esc_html__( 'Flip Vertical', 'hemelios' )        => 'wpb_flipInX',
			esc_html__( 'Flip Horizontal', 'hemelios' )      => 'wpb_flipInY',
			esc_html__( 'Bounce', 'hemelios' )               => 'wpb_bounce',
			esc_html__( 'Flash', 'hemelios' )                => 'wpb_flash',
			esc_html__( 'Shake', 'hemelios' )                => 'wpb_shake',
			esc_html__( 'Pulse', 'hemelios' )                => 'wpb_pulse',
			esc_html__( 'Swing', 'hemelios' )                => 'wpb_swing',
			esc_html__( 'Rubber band', 'hemelios' )          => 'wpb_rubberBand',
			esc_html__( 'Wobble', 'hemelios' )               => 'wpb_wobble',
			esc_html__( 'Tada', 'hemelios' )                 => 'wpb_tada'
		),
		'description' => esc_html__( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'hemelios' ),
		'group'       => esc_html__( 'Animation Settings', 'hemelios' )
	);

	// Add animation duration
	$add_duration_animation = array(
		'type'        => 'textfield',
		'heading'     => esc_html__( 'Animation Duration', 'hemelios' ),
		'param_name'  => 'duration',
		'value'       => '',
		'description' => esc_html__( 'Duration in seconds. You can use decimal points in the value. Use this field to specify the amount of time the animation plays. <em>The default value depends on the animation, leave blank to use the default.</em>', 'hemelios' ),
		'dependency'  => Array(
			'element' => 'css_animation',
			'value'   => array(
				'wpb_fadeIn',
				'wpb_fadeInDown',
				'wpb_fadeInUp',
				'wpb_fadeInLeft',
				'wpb_fadeInRight',
				'wpb_bounceIn',
				'wpb_bounceInDown',
				'wpb_bounceInUp',
				'wpb_bounceInLeft',
				'wpb_bounceInRight',
				'wpb_zoomIn',
				'wpb_flipInX',
				'wpb_flipInY',
				'wpb_bounce',
				'wpb_flash',
				'wpb_shake',
				'wpb_pulse',
				'wpb_swing',
				'wpb_rubberBand',
				'wpb_wobble',
				'wpb_tada'
			)
		),
		'group'       => esc_html__( 'Animation Settings', 'hemelios' )
	);

	// Add animation delay time
	$add_delay_animation = array(
		'type'        => 'textfield',
		'heading'     => esc_html__( 'Animation Delay', 'hemelios' ),
		'param_name'  => 'delay',
		'value'       => '',
		'description' => esc_html__( 'Delay in seconds. You can use decimal points in the value. Use this field to delay the animation for a few seconds, this is helpful if you want to chain different effects one after another above the fold.', 'hemelios' ),
		'dependency'  => Array(
			'element' => 'css_animation',
			'value'   => array(
				'wpb_fadeIn',
				'wpb_fadeInDown',
				'wpb_fadeInUp',
				'wpb_fadeInLeft',
				'wpb_fadeInRight',
				'wpb_bounceIn',
				'wpb_bounceInDown',
				'wpb_bounceInUp',
				'wpb_bounceInLeft',
				'wpb_bounceInRight',
				'wpb_zoomIn',
				'wpb_flipInX',
				'wpb_flipInY',
				'wpb_bounce',
				'wpb_flash',
				'wpb_shake',
				'wpb_pulse',
				'wpb_swing',
				'wpb_rubberBand',
				'wpb_wobble',
				'wpb_tada'
			)
		),
		'group'       => esc_html__( 'Animation Settings', 'hemelios' )
	);

	// Row layout
	$params_row = array(
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Layout', 'hemelios' ),
			'param_name' => 'layout',
			'value'      => array(
				esc_html__( 'Full Width', 'hemelios' )      => 'wide',
				esc_html__( 'Container', 'hemelios' )       => 'boxed',
				esc_html__( 'Container Fluid', 'hemelios' ) => 'container-fluid',
			),
		),
		array(
			'type'        => 'checkbox',
			'heading'     => esc_html__( 'Full height row?', 'js_composer' ),
			'param_name'  => 'full_height',
			'description' => esc_html__( 'If checked row will be set to full height.', 'js_composer' ),
			'value'       => array( esc_html__( 'Yes', 'js_composer' ) => 'yes' )
		),
		array(
			'type'        => 'checkbox',
			'heading'     => esc_html__( 'Use Flex Row', 'js_composer' ),
			'param_name'  => 'flex_row',
			'description' => esc_html__( 'Use flex row, inner columns will have the same height.', 'js_composer' ),
			'value'       => array( esc_html__( 'Yes', 'js_composer' ) => 'yes' )
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Content position', 'js_composer' ),
			'param_name'  => 'content_placement',
			'value'       => array(
				esc_html__( 'Middle', 'js_composer' ) => 'middle',
				esc_html__( 'Top', 'js_composer' )    => 'top',
			),
			'description' => esc_html__( 'Select content position within row.', 'js_composer' ),
			'dependency'  => array(
				'element'   => 'full_height',
				'not_empty' => true,
			),
		),
		array(
			'type'        => 'checkbox',
			'heading'     => esc_html__( 'Use video background?', 'js_composer' ),
			'param_name'  => 'video_bg',
			'description' => esc_html__( 'If checked, video will be used as row background.', 'js_composer' ),
			'value'       => array( esc_html__( 'Yes', 'js_composer' ) => 'yes' )
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'YouTube link', 'js_composer' ),
			'param_name'  => 'video_bg_url',
			'value'       => 'https://www.youtube.com/watch?v=lMJXxhRFO1k', // default video url
			'description' => esc_html__( 'Add YouTube link.', 'js_composer' ),
			'dependency'  => array(
				'element'   => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Parallax', 'js_composer' ),
			'param_name'  => 'video_bg_parallax',
			'value'       => array(
				esc_html__( 'None', 'js_composer' )      => '',
				esc_html__( 'Simple', 'js_composer' )    => 'content-moving',
				esc_html__( 'With fade', 'js_composer' ) => 'content-moving-fade',
			),
			'description' => esc_html__( 'Add parallax type background for row.', 'js_composer' ),
			'dependency'  => array(
				'element'   => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Parallax', 'js_composer' ),
			'param_name'  => 'parallax',
			'value'       => array(
				esc_html__( 'None', 'js_composer' )      => '',
				esc_html__( 'Simple', 'js_composer' )    => 'content-moving',
				esc_html__( 'With fade', 'js_composer' ) => 'content-moving-fade',
			),
			'description' => esc_html__( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'js_composer' ),
			'dependency'  => array(
				'element'  => 'video_bg',
				'is_empty' => true,
			),
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Image', 'js_composer' ),
			'param_name'  => 'parallax_image',
			'value'       => '',
			'description' => esc_html__( 'Select image from media library.', 'js_composer' ),
			'dependency'  => array(
				'element'   => 'parallax',
				'not_empty' => true,
			),
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Parallax speed', 'hemelios' ),
			'param_name' => 'parallax_speed',
			'value'      => '1.5',
			'dependency' => Array(
				'element' => 'parallax',
				'value'   => array( 'content-moving', 'content-moving-fade' )
			),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Show background overlay', 'hemelios' ),
			'param_name'  => 'overlay_set',
			'description' => esc_html__( 'Hide or Show overlay on background images.', 'hemelios' ),
			'value'       => array(
				esc_html__( 'Hide, please', 'hemelios' )       => 'hide_overlay',
				esc_html__( 'Show Overlay Color', 'hemelios' ) => 'show_overlay_color',
				esc_html__( 'Show Overlay Image', 'hemelios' ) => 'show_overlay_image',
			)
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Image Overlay:', 'hemelios' ),
			'param_name'  => 'overlay_image',
			'value'       => '',
			'description' => esc_html__( 'Upload image overlay.', 'hemelios' ),
			'dependency'  => Array( 'element' => 'overlay_set', 'value' => array( 'show_overlay_image' ) ),
		),
		array(
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Overlay color', 'hemelios' ),
			'param_name'  => 'overlay_color',
			'description' => esc_html__( 'Select color for background overlay.', 'hemelios' ),
			'value'       => '',
			'dependency'  => Array( 'element' => 'overlay_set', 'value' => array( 'show_overlay_color' ) ),
		),
		array(
			'type'        => 'number',
			'class'       => '',
			'heading'     => esc_html__( 'Overlay opacity', 'hemelios' ),
			'param_name'  => 'overlay_opacity',
			'value'       => '50',
			'min'         => '1',
			'max'         => '100',
			'suffix'      => '%',
			'description' => esc_html__( 'Select opacity for overlay.', 'hemelios' ),
			'dependency'  => Array(
				'element' => 'overlay_set',
				'value'   => array( 'show_overlay_color', 'show_overlay_image' )
			),
		),
		array(
			'type'        => 'el_id',
			'heading'     => esc_html__( 'Row ID', 'js_composer' ),
			'param_name'  => 'el_id',
			'description' => sprintf( esc_html__( 'Enter row ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'js_composer' ), 'http://www.w3schools.com/tags/att_global_id.asp' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name'  => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
		),
		array(
			'type'       => 'css_editor',
			'heading'    => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group'      => esc_html__( 'Design Options', 'js_composer' )
		),
		$add_css_animation,
		$add_duration_animation,
		$add_delay_animation,
	);

	vc_map( array(
		'name'                    => esc_html__( 'Row', 'hemelios' ),
		'base'                    => 'vc_row',
		'is_container'            => true,
		'icon'                    => 'icon-wpb-row',
		'show_settings_on_create' => false,
		'category'                => esc_html__( 'Content', 'hemelios' ),
		'description'             => esc_html__( 'Place content elements inside the row', 'hemelios' ),
		'params'                  => $params_row,
		'js_view'                 => 'VcRowView'
	) );
	vc_map( array(
		'name'                    => esc_html__( 'Row', 'hemelios' ), //Inner Row
		'base'                    => 'vc_row_inner',
		'content_element'         => false,
		'is_container'            => true,
		'icon'                    => 'icon-wpb-row',
		'weight'                  => 1000,
		'show_settings_on_create' => false,
		'description'             => esc_html__( 'Place content elements inside the row', 'hemelios' ),
		'params'                  => $params_row,
		'js_view'                 => 'VcRowView'
	) );


	// Accordion
	g5plus_vc_update_single_param_value( 'vc_tta_section', 'i_type', __( 'Hemelios', 'js_composer' ), 'hemelios' );

	// === Progress bar custom vc_map ===
	// Add and update param

	$progress_additional_param = array(
		// Update
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Widget title', 'js_composer' ),
			'param_name'  => 'title',
			'description' => __( 'Enter text used as widget title (Note: located above content element).', 'js_composer' ),
			'weight'      => 10
		),
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Color', 'js_composer' ),
			'param_name'  => 'bgcolor',
			'value'       => array(
				__( 'Default', 'js_composer' )      => 'default',
				__( 'Custom Color', 'js_composer' ) => 'custom',
			),
			'description' => __( 'Select bar background color, default color is primary color.', 'js_composer' ),
		),
		array(
			'type'        => 'colorpicker',
			'heading'     => __( 'Bar custom background color', 'js_composer' ),
			'param_name'  => 'custombgcolor',
			'description' => __( 'Select custom background color for bars.', 'js_composer' ),
			'dependency'  => array(
				'element' => 'bgcolor',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'        => 'colorpicker',
			'heading'     => __( 'Bar custom text color', 'js_composer' ),
			'param_name'  => 'customtxtcolor',
			'description' => __( 'Select custom text color for bars.', 'js_composer' ),
			'dependency'  => array(
				'element' => 'bgcolor',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Extra class name', 'hemelios' ),
			'param_name'  => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.
Close', 'hemelios' ),
			'weight'      => - 1
		),
		// Add new
		array(
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Trailing background color', 'hemelios' ),
			'param_name'  => 'trailing_bg_color',
			'value'       => '#eee',
			'description' => esc_html__( 'Trailing background color', 'hemelios' ),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Bar title position', 'hemelios' ),
			'param_name'  => 'bar_title_position',
			'value'       => array(
				__( 'Default', 'hemelios' ) => 'default',
				__( 'Bottom', 'hemelios' )  => 'bottom',
			),
			'weight'      => 8,
			'description' => esc_html__( 'Config bar title position', 'hemelios' ),
		),
	);
	vc_add_params( 'vc_progress_bar', $progress_additional_param );

}

add_action( 'vc_after_init', 'register_vc_map' );