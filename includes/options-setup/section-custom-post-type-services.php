<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

$services_title_bg_url     = get_template_directory_uri() . '/assets/images/bg-portfolio-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Services', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-asterisk',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'service_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select page layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),
			array(
				'id'       => 'service_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'left'
			),

			array(
				'id'       => 'service_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'service_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),


			array(
				'id'       => 'service_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-services',
				'required' => array( 'service_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'service_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-2',
				'required' => array( 'service_sidebar', '=', array( 'right', 'both' ) ),
			),
			array(
				'id'       => 'show_service_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Services Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable services title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'service_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Services Title Height', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px) or empty.', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the services title here.', 'hemelios' ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '420'
				)
			),
			array(
				'id'       => 'breadcrumbs_in_service_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Services Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in services title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),

			array(
				'id'       => 'service_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Services Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select services breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_service_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'service_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Services Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload services title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $services_title_bg_url
				)
			),

		)
	) );