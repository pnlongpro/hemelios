<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/8/2015
 * Time: 3:24 PM
 */
if ( class_exists( 'WooCommerce' ) ) {
	/*================================================
	HOOKS
	================================================== */
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
	add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 5 );


	/*================================================
	RESET LOOP
	================================================== */
	if ( !function_exists( 'g5plus_woocommerce_reset_loop' ) ) {
		function g5plus_woocommerce_reset_loop() {
			global $g5plus_woocommerce_loop;
			$g5plus_woocommerce_loop['layout']         = '';
			$g5plus_woocommerce_loop['single_columns'] = '';
			$g5plus_woocommerce_loop['columns']        = '';
		}
	}

	/*================================================
	LOOP CATEGORY TEMPLATE
	================================================== */
	if ( !function_exists( 'g5plus_woocommerce_template_loop_category' ) ) {
		function g5plus_woocommerce_template_loop_category() {
			wc_get_template( 'loop/category.php' );
		}

		add_action( 'woocommerce_after_shop_loop_item_title', 'g5plus_woocommerce_template_loop_category', 1 );
	}

	/*================================================
	LOOP NAME TEMPLATE
	================================================== */
	if ( !function_exists( 'g5plus_woocommerce_template_loop_name' ) ) {
		function g5plus_woocommerce_template_loop_name() {
			wc_get_template( 'loop/name.php' );
		}

		add_action( 'woocommerce_after_shop_loop_item_title', 'g5plus_woocommerce_template_loop_name', 1 );
	}

	/*================================================
	LOOP LINK TEMPLATE
	================================================== */
	if ( !function_exists( 'g5plus_woocomerce_template_loop_link' ) ) {
		function g5plus_woocomerce_template_loop_link() {
			wc_get_template( 'loop/link.php' );
		}

		add_action( 'woocommerce_before_shop_loop_item_title', 'g5plus_woocomerce_template_loop_link', 20 );
	}

	/*================================================
	QUICK VIEW TEMPLATE
	================================================== */
	if ( !function_exists( 'g5plus_woocomerce_template_loop_quick_view' ) ) {
		function g5plus_woocomerce_template_loop_quick_view() {
			wc_get_template( 'loop/quick-view.php' );
		}

		add_action( 'woocommerce_before_shop_loop_item_title', 'g5plus_woocomerce_template_loop_quick_view', 15 );
	}


	/*================================================
	FILTER PRODUCTS PER PAGE
	================================================== */
	if ( !function_exists( 'g5plus_show_products_per_page' ) ) {
		function g5plus_show_products_per_page() {
			$g5plus_options   = g5plus_option();
			$product_per_page = $g5plus_options['product_per_page'];
			if ( empty( $product_per_page ) ) {
				$product_per_page = 12;
			}
			$page_size = isset( $_GET['page_size'] ) ? wc_clean( $_GET['page_size'] ) : $product_per_page;

			return $page_size;
		}

		add_filter( 'loop_shop_per_page', 'g5plus_show_products_per_page' );
	}


	/*================================================
	OVERWRITE LOOP PRODUCT THUMBNAIL
	================================================== */
	if ( !function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
		/**
		 * Get the product thumbnail for the loop.
		 *
		 * @access        public
		 * @subpackage    Loop
		 * @return void
		 */
		function woocommerce_template_loop_product_thumbnail() {
			?>
			<?php if ( has_post_thumbnail() ) : ?>
				<div class="product-thumb-primary">
					<?php echo woocommerce_get_product_thumbnail(); ?>
				</div>
			<?php endif; ?>
			<?php
		}
	}

	/*================================================
	PRODUCT NEW AND AUTHOR
	================================================== */
	if ( !function_exists( 'g5plus_woocommerce_add_custom_general_fields' ) ) {
		function g5plus_woocommerce_add_custom_general_fields() {
			echo '<div class="options_group">';
			woocommerce_wp_checkbox(
				array(
					'id'    => 'g5plus_product_new',
					'label' => esc_html__( 'New Product', 'hemelios' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id'    => 'g5plus_product_author',
					'label' => esc_html__( 'Product Author', 'hemelios' )
				)
			);
			echo '</div>';
		}

		add_action( 'woocommerce_product_options_general_product_data', 'g5plus_woocommerce_add_custom_general_fields' );
	}


	/*
	 * UPDATE PRODUCT META
	 */
	if ( !function_exists( 'g5plus_woo_add_custom_general_fields_save' ) ) {
		function g5plus_woo_add_custom_general_fields_save( $post_id ) {
			$g5plus_product_new    = isset( $_POST['g5plus_product_new'] ) ? 'yes' : 'no';
			$g5plus_product_author = isset( $_POST['g5plus_product_author'] ) ? $_POST['g5plus_product_author'] : '';
			update_post_meta( $post_id, 'g5plus_product_new', $g5plus_product_new );
			update_post_meta( $post_id, 'g5plus_product_author', $g5plus_product_author );
		}

		add_action( 'woocommerce_process_product_meta', 'g5plus_woo_add_custom_general_fields_save' );
	}

	if ( !function_exists( 'g5plus_columns_into_product_list' ) ) {
		function g5plus_columns_into_product_list( $defaults ) {
			$defaults['g5plus_product_new'] = 'New';

			return $defaults;
		}

		add_filter( 'manage_edit-product_columns', 'g5plus_columns_into_product_list' );
	}

	if ( !function_exists( 'g5plus_column_into_product_list' ) ) {
		function g5plus_column_into_product_list( $column, $post_id ) {
			switch ( $column ) {
				case 'g5plus_product_new':
					echo get_post_meta( $post_id, 'g5plus_product_new', true );
					break;
			}
		}

		add_action( 'manage_product_posts_custom_column', 'g5plus_column_into_product_list', 10, 2 );
	}

	if ( !function_exists( 'g5plus_sortable_columns' ) ) {
		function g5plus_sortable_columns() {
			return array(
				'g5plus_product_new' => 'g5plus_product_new'
			);
		}

		add_filter( "manage_edit-product_sortable_columns", "g5plus_sortable_columns" );
	}

	if ( !function_exists( 'g5plus_event_column_orderby' ) ) {
		function g5plus_event_column_orderby( $query ) {
			if ( !is_admin() ) {
				return;
			}
			$orderby = $query->get( 'orderby' );
			if ( 'g5plus_product_new' == $orderby ) {
				$query->set( 'meta_key', 'g5plus_product_new' );
				$query->set( 'orderby', 'meta_value_num' );
			}
		}

		add_action( 'pre_get_posts', 'g5plus_event_column_orderby' );
	}


	/*================================================
	SINGLE PRODUCT
	================================================== */
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );

	if ( !function_exists( 'g5plus_product_description_heading' ) ) {
		function g5plus_product_description_heading() {
			return '';
		}
	}
	add_filter( 'woocommerce_product_description_heading', 'g5plus_product_description_heading' );
	add_filter( 'woocommerce_product_additional_information_heading', 'g5plus_product_description_heading' );


	if ( !function_exists( 'g5plus_related_products_args' ) ) {
		function g5plus_related_products_args() {
			$g5plus_options         = g5plus_option();
			$args['posts_per_page'] = isset( $g5plus_options['related_product_count'] ) ? $g5plus_options['related_product_count'] : 8;

			return $args;
		}

		add_filter( 'woocommerce_output_related_products_args', 'g5plus_related_products_args' );
	}

	if ( !function_exists( 'g5plus_woocommerce_product_related_posts_relate_by_category' ) ) {
		function g5plus_woocommerce_product_related_posts_relate_by_category() {
			$g5plus_options = g5plus_option();

			return $g5plus_options['related_product_condition']['category'] == 1 ? true : false;
		}

		add_filter( 'woocommerce_product_related_posts_relate_by_category', 'g5plus_woocommerce_product_related_posts_relate_by_category' );
	}

	if ( !function_exists( 'g5plus_woocommerce_product_related_posts_relate_by_tag' ) ) {
		function g5plus_woocommerce_product_related_posts_relate_by_tag() {
			$g5plus_options = g5plus_option();

			return $g5plus_options['related_product_condition']['tag'] == 1 ? true : false;
		}

		add_filter( 'woocommerce_product_related_posts_relate_by_tag', 'g5plus_woocommerce_product_related_posts_relate_by_tag' );
	}
	/*
	 * PRODUCT AUTHOR
	 */

	if ( !function_exists( 'g5plus_woocommerce_template_product_author' ) ) {
		function g5plus_woocommerce_template_product_author() {
			wc_get_template( 'single-product/author.php' );
		}

		add_action( 'woocommerce_single_product_summary', 'g5plus_woocommerce_template_product_author', 7 );
		add_action( 'woocommerce_after_shop_loop_item_title', 'g5plus_woocommerce_template_product_author', 4 );
	}


	/*================================================
	SHOPPING CART
	================================================== */
	remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
	add_action( 'woocommerce_after_cart', 'woocommerce_cross_sell_display', 15 );
	add_action( 'woocommerce_before_cart_totals', 'woocommerce_shipping_calculator', 5 );

	if ( !function_exists( 'g5plus_button_continue_shopping' ) ) {
		function g5plus_button_continue_shopping() {
			$continue_shopping = get_permalink( wc_get_page_id( 'shop' ) );
			?>
			<a href="<?php echo esc_url( $continue_shopping ); ?>" class="continue-shopping button"><?php echo esc_html__( 'Continue shopping', 'hemelios' ); ?></a>
			<?php
		}
	}
	if ( !function_exists( 'g5plus_woocommerce_sale_flash' ) ) {
		function g5plus_woocommerce_sale_flash( $sale_flash, $post, $product ) {
			$g5plus_options          = g5plus_option();
			$product_sale_flash_mode = isset( $g5plus_options['product_sale_flash_mode'] ) ? $g5plus_options['product_sale_flash_mode'] : '';
			if ( $product_sale_flash_mode == 'percent' ) {
				$sale_percent = 0;
				if ( $product->is_on_sale() && $product->product_type != 'grouped' ) {
					if ( $product->product_type == 'variable' ) {
						$available_variations = $product->get_available_variations();
						for ( $i = 0; $i < count( $available_variations ); ++ $i ) {
							$variation_id      = $available_variations[$i]['variation_id'];
							$variable_product1 = new WC_Product_Variation( $variation_id );
							$regular_price     = $variable_product1->get_regular_price();
							$sales_price       = $variable_product1->get_sale_price();
							$price             = $variable_product1->get_price();
							if ( $sales_price != $regular_price && $sales_price == $price ) {
								$percentage = round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ), 1 );
								if ( $percentage > $sale_percent ) {
									$sale_percent = $percentage;
								}
							}
						}
					} else {
						$sale_percent = round( ( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ), 1 );
					}
				}
				if ( $sale_percent > 0 ) {
					return '<span class="on-sale">' . $sale_percent . '%</span>';
				} else {
					return "";
				}

			}

			return $sale_flash;
		}

		add_filter( 'woocommerce_sale_flash', 'g5plus_woocommerce_sale_flash', 10, 3 );
	}


	/*Quick View*/
	add_action( 'g5plus_before_quick_view_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	if ( !function_exists( 'g5plus_quick_view_product_images' ) ) {
		function g5plus_quick_view_product_images() {
			wc_get_template( 'quick-view/product-image.php' );
		}

		add_action( 'g5plus_before_quick_view_product_summary', 'g5plus_quick_view_product_images', 20 );
	}


	if ( !function_exists( 'g5plus_template_quick_view_product_title' ) ) {
		function g5plus_template_quick_view_product_title() {
			wc_get_template( 'quick-view/title.php' );
		}

		add_action( 'g5plus_quick_view_product_summary', 'g5plus_template_quick_view_product_title', 5 );
	}

	if ( !function_exists( 'g5plus_template_quick_view_product_rating' ) ) {
		function g5plus_template_quick_view_product_rating() {
			wc_get_template( 'quick-view/rating.php' );
		}

		add_action( 'g5plus_quick_view_product_summary', 'g5plus_template_quick_view_product_rating', 10 );
	}

	add_action( 'g5plus_quick_view_product_summary', 'woocommerce_template_single_price', 15 );
	add_action( 'g5plus_quick_view_product_summary', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'g5plus_quick_view_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'g5plus_quick_view_product_summary', 'woocommerce_template_single_meta', 40 );
	add_action( 'g5plus_quick_view_product_summary', 'woocommerce_template_single_sharing', 50 );


	/*
	 * DISPLAY PRICE
	 */
	if ( !function_exists( 'g5plus_reformat_price_html' ) ) {
		function g5plus_reformat_price_html( $price, $from, $to, $this ) {
			$price = '<ins>' . ( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) . '</ins><del>' . ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) . '</del>';

			return $price;
		}

		add_filter( 'woocommerce_get_price_html_from_to', 'g5plus_reformat_price_html', 10, 4 );
	}
	if (!function_exists('g5plus_woocommerce_clear_cart_url')) {
		// check for empty-cart get param to clear the cart
		add_action('init', 'g5plus_woocommerce_clear_cart_url');
		function g5plus_woocommerce_clear_cart_url() {
			global $woocommerce;
			if (isset($_GET['empty-cart'])) {
				$woocommerce->cart->empty_cart();
			}
		}
	}
}
