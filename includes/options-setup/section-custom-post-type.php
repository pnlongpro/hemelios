<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Custom Post Type', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-screenshot',
		'fields' => array(
			array(
				'id'       => 'cpt-disable',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Disable Custom Post Types', 'hemelios' ),
				'subtitle' => esc_html__( 'You can disable the custom post types used within the theme here, by checking the corresponding box. note: if you do not want to disable any, then make sure none of the boxes are checked.', 'hemelios' ),
				'options'  => array(
					'portfolio' => 'Portfolio',
					'services'  => 'Services',
					'ourteam'   => 'Our Team',
				),
				'default'  => array(
					'portfolio' => '0',
					'services'  => '0',
					'ourteam'   => '0',
				)
			),

			array(
				'id'   => 'custom-post-type-divide-0',
				'type' => 'divide'
			),


		)
	) );