<?php
if ( !function_exists( 'g5plus_register_sidebar' ) ) {
	function g5plus_register_sidebar() {
		register_sidebar( array(
			'name'          => esc_html__( "Sidebar 1", 'hemelios' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( "Widget Area 1", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( "Sidebar 2", 'hemelios' ),
			'id'            => 'sidebar-2',
			'description'   => esc_html__( "Widget Area 2", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Footer 1", 'hemelios' ),
			'id'            => 'footer-1',
			'description'   => esc_html__( "Footer 1", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Footer 2", 'hemelios' ),
			'id'            => 'footer-2',
			'description'   => esc_html__( "Footer 2", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Footer 3", 'hemelios' ),
			'id'            => 'footer-3',
			'description'   => esc_html__( "Footer 3", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Footer 4", 'hemelios' ),
			'id'            => 'footer-4',
			'description'   => esc_html__( "Footer 4", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Footer 1", 'hemelios' ),
			'id'            => 'top-footer-1',
			'description'   => esc_html__( "Top Footer 1", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Footer 2", 'hemelios' ),
			'id'            => 'top-footer-2',
			'description'   => esc_html__( "Top Footer 2", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Footer 3", 'hemelios' ),
			'id'            => 'top-footer-3',
			'description'   => esc_html__( "Top Footer 3", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Footer 4", 'hemelios' ),
			'id'            => 'top-footer-4',
			'description'   => esc_html__( "Top Footer 4", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );


		register_sidebar( array(
			'name'          => esc_html__( "Bottom Bar Left", 'hemelios' ),
			'id'            => 'bottom_bar_left',
			'description'   => esc_html__( "Bottom Bar Left", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Bottom Bar Right", 'hemelios' ),
			'id'            => 'bottom_bar_right',
			'description'   => esc_html__( "Bottom Bar Right", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "WooCommerce Sidebar", 'hemelios' ),
			'id'            => 'woocommerce',
			'description'   => esc_html__( "WooCommerce", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Bar Left", 'hemelios' ),
			'id'            => 'top_bar_left',
			'description'   => esc_html__( "Top Bar Left", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Top Bar Right", 'hemelios' ),
			'id'            => 'top_bar_right',
			'description'   => esc_html__( "Top Bar Right", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( "Portfolio Sidebar", 'hemelios' ),
			'id'            => 'portfolio',
			'description'   => esc_html__( "Portfolio Sidebar", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( "Page Sidebar", 'hemelios' ),
			'id'            => 'sidebar-page',
			'description'   => esc_html__( "Page Sidebar", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( "Services Sidebar", 'hemelios' ),
			'id'            => 'sidebar-services',
			'description'   => esc_html__( "Page Services", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( "Services Bottom Sidebar", 'hemelios' ),
			'id'            => 'sidebar-services-bottom',
			'description'   => esc_html__( "This appear in the bottom of single services page.", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( "Page Bottom Sidebar", 'hemelios' ),
			'id'            => 'sidebar-page-bottom',
			'description'   => esc_html__( "This appear in the bottom of general page.", 'hemelios' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );
	}

	add_action( 'widgets_init', 'g5plus_register_sidebar' );

}

if ( !function_exists( 'g5plus_redux_custom_widget_area_filter' ) ) {
	function g5plus_redux_custom_widget_area_filter( $arg ) {
		return array(
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>'
		);
	}

	add_filter( 'redux_custom_widget_args', 'g5plus_redux_custom_widget_area_filter' );
}
