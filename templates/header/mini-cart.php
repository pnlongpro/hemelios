<?php
$g5plus_options = g5plus_option();
$icon_shopping_cart_class = array( 'shopping-cart-wrapper', 'header-customize-item' );
if ( $g5plus_options['mobile_header_shopping_cart'] == '0' ) {
	$icon_shopping_cart_class[] = 'mobile-hide-shopping-cart';
}

// GET VIEW CART SUBTOTAL OPTION
$view_cart_subtotal = g5plus_get_post_meta_box_option('g5plus_cart_subtotal' );


if ($view_cart_subtotal == '' || $view_cart_subtotal == '-1') {
	if ( isset( $g5plus_options['view_cart_subtotal'] ) ) {
		$view_cart_subtotal = $g5plus_options['view_cart_subtotal'];
	} else {
		$view_cart_subtotal = '0';
	}
}

if (is_404()) {
	$view_cart_subtotal = '0';
}

//GET VIEW CART SUBTOTAL CLASS
if ( $view_cart_subtotal == '0' ) {
	$icon_shopping_cart_class[] = 'disable-cart-subtotal';
}



?>
<div class="<?php echo join( ' ', $icon_shopping_cart_class ); ?>">
	<div class="widget_shopping_cart_content ">
		<?php get_template_part( 'woocommerce/cart/mini-cart' ); ?>
	</div>
</div>