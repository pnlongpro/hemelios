<?php
/**
 * Created by PhpStorm.
 * User: hoantv
 * Date: 2014-11-12
 * Time: 10:23 AM
 */

function g5plus_create_setting_file() {
	global $wpdb;
	$options_key = array(
		'sidebars_widgets'                         => '=',
		'widget_%'                                 => 'like',
		'theme_mods_' . get_option( 'stylesheet' ) => '=',
		'g5plus_hemelios_options'                  => '=',
		'show_on_front'                            => '=',
		'page_on_front'                            => '=',
		'page_for_posts'                           => '=',
		'shop_catalog_image_size'                  => '=',
		'shop_single_image_size'                   => '=',
		'shop_thumbnail_image_size'                => '=',
		'yith_wcwl_button_position'                => '=',
		'slickr_flickr_options'                    => '=',
		"woocommerce_%"                            => "like",
		"yith_woocompare_%"                        => "like",
		'mc4wp_lite_form'                          => '=',
		'permalink_structure'                      => '=',
		'medium_size_w'                            => '=',
		'medium_size_h'                            => '=',
		'thumbnail_size_w'                         => '=',
		'thumbnail_size_h'                         => '=',
		'large_size_w'                             => '=',
		'large_size_h'                             => '=',
		'xmenu_menu_item_widget_areas'             => '=',
		'xmenu_setting_option%'                    => 'like',
	);

	$file_data = array();
	foreach ( $options_key as $key => $value ) {
		$rows = $wpdb->get_results( $wpdb->prepare( "SELECT option_name, option_value, autoload FROM $wpdb->options WHERE option_name $value %s", $key ) );

		foreach ( $rows as $row ) {
			$file_data[$row->option_name] = array(
				"autoload"     => $row->autoload,
				"option_value" => base64_encode( $row->option_value )
			);
		}
	}

	file_put_contents( get_template_directory()  . '/assets/cache/setting.json', json_encode( $file_data ) );
}

function g5plus_create_change_data_file() {
	$data = array();
	$navs = get_terms( 'nav_menu', array(
		'hide_empty' => 1,
	) );

	foreach ( $navs as $key => $value ) {
		$data['nav_menu'][$value->slug] = $value->term_id;
	}
	unset( $navs );

	$product_cat = get_terms( 'product_cat', array(
		'hide_empty' => 1,
	) );
	foreach ( $product_cat as $key => $value ) {
		$data['product_cat'][$value->slug] = $value->term_id;
	}
	unset( $categorys );

	$categorys = get_terms( array( 'category' ), array( 'hide_empty' => 1, ) );
	foreach ( $categorys as $term ) {
		$data['category'][$term->slug] = $term->term_id;
	}

	// Set page
	$data['page']['page_for_posts'] = get_option( 'page_for_posts' );
	$data['page']['page_on_front']  = get_option( 'page_on_front' );

	foreach ( $data['page'] as $key => $value ) {
		$post = get_post( $value );
		if ( isset( $post ) && $post ) {
			$data['page-slug'][$post->post_name] = $value;
		}
	}

	// GET TAX-META-CLASS CATEGORY
	global $wpdb;

	$rows = $wpdb->get_results( "select option_name, option_value from $wpdb->options where (option_name like 'tax_meta_%')" );
	foreach ( $rows as $row ) {
		$data['tax-meta-class'][$row->option_name] = $row->option_value;
	}

	$rows = $wpdb->get_results( "select woocommerce_term_id, meta_value from wp_woocommerce_termmeta where (meta_key = 'thumbnail_id')" );
	foreach ( $rows as $row ) {
		$data['woocommerce_termmeta'][$row->woocommerce_term_id] = $row->meta_value;
		if ( $row->meta_value > 0 ) {
			$post                                = get_post( $row->meta_value );
			$data['page-slug'][$post->post_name] = $row->meta_value;
		}
	}

	file_put_contents( get_template_directory()  . '/assets/cache/change-data.json', json_encode( $data ) );

}

if ( in_array( $GLOBALS['pagenow'], array( 'themes.php' ) ) ) {
	if ( isset( $_REQUEST['demodata'] ) ) {
		$demodata = $_REQUEST['demodata'];
		if ( $demodata == '1' ) {
			g5plus_create_setting_file();
			add_action( 'init', 'g5plus_create_change_data_file', 100 );
		}
	}
}
