<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Custom Favicon', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-eye-open',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'custom_favicon',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Custom favicon', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a 16px x 16px png/gif/ico image that will represent your website favicon', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => get_template_directory_uri() . '/assets/images/favicon.ico'
				)
			),
			array(
				'id'       => 'custom_ios_title',
				'type'     => 'text',
				'title'    => esc_html__( 'Custom iOS Bookmark Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enter a custom title for your site for when it is added as an ios bookmark.', 'hemelios' ),
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'custom_ios_icon57',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Custom iOS 57x57', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a 57px x 57px png image that will be your website bookmark on non-retina ios devices.', 'hemelios' ),
				'desc'     => ''
			),
			array(
				'id'       => 'custom_ios_icon72',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Custom iOS 72x72', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a 72px x 72px png image that will be your website bookmark on non-retina ios devices.', 'hemelios' ),
				'desc'     => ''
			),
			array(
				'id'       => 'custom_ios_icon114',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Custom iOS 114x114', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a 114px x 114px png image that will be your website bookmark on retina ios devices.', 'hemelios' ),
				'desc'     => ''
			),
			array(
				'id'       => 'custom_ios_icon144',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Custom iOS 144x144', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload a 144px x 144px png image that will be your website bookmark on retina ios devices.', 'hemelios' ),
				'desc'     => ''
			),
		)
	) );