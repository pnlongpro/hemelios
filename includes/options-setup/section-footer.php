<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Footer', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-website',
		'fields' => array(
			array(
				'id'       => 'top_footer_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Top Footer Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the top footer column layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'footer-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-1.jpg' ),
					'footer-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-2.jpg' ),
					'footer-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-3.jpg' ),
					'footer-4' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-4.jpg' ),
					'footer-5' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-5.jpg' ),
					'footer-6' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-6.jpg' ),
					'footer-7' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-7.jpg' ),
					'footer-8' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-8.jpg' ),
					'footer-9' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-9.jpg' ),
				),
				'default'  => 'footer-1'
			),
			array(
				'id'       => 'footer_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Footer Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the footer column layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'footer-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-1.jpg' ),
					'footer-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-2.jpg' ),
					'footer-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-3.jpg' ),
					'footer-4' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-4.jpg' ),
					'footer-5' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-5.jpg' ),
					'footer-6' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-6.jpg' ),
					'footer-7' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-7.jpg' ),
					'footer-8' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-8.jpg' ),
					'footer-9' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-9.jpg' ),
				),
				'default'  => 'footer-1'
			),

			array(
				'id'       => 'footer_bg_image',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Footer Background Image', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload footer background image here.', 'hemelios' ),
				'desc'     => '',
				'default'  => ''
			),

			array(
				'id'       => 'footer_parallax',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Footer Parallax', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable footer parallax.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0'
			),

			array(
				'id'       => 'collapse_footer',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Collapse footer on mobile device', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable collapse footer.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0'
			),

			array(
				'id'   => 'footer_divide',
				'type' => 'divide'
			),

			array(
				'id'       => 'footer_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Footer Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set footer background color.', 'hemelios' ),
				'default'  => '#222',
				'validate' => 'color',
			),

			array(
				'id'       => 'footer_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Footer Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set footer text color.', 'hemelios' ),
				'default'  => '#888888',
				'validate' => 'color',
			),

			array(
				'id'       => 'footer_heading_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Footer Heading Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set footer heading text color.', 'hemelios' ),
				'default'  => '#FFFFFF',
				'validate' => 'color',
			),

			array(
				'id'     => 'section-footer-layout',
				'type'   => 'section',
				'title'  => esc_html__( 'Bottom Bar', 'hemelios' ),
				'indent' => true
			),

			array(
				'id'       => 'bottom_bar',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Bottom Bar', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable bottom bar (below footer)', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),

			array(
				'id'       => 'bottom_bar_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Bottom bar Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the bottom bar column layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'bottom-bar-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-1.jpg' ),
					'bottom-bar-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-2.jpg' ),
					'bottom-bar-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-3.jpg' ),
					'bottom-bar-4' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/bottom-bar-layout-4.jpg' ),
				),
				'default'  => 'bottom-bar-1',
				'required' => array( 'bottom_bar', '=', '1' ),
			),

			array(
				'id'       => 'bottom_bar_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Bottom Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default bottom left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'bottom_bar_left',
				'required' => array( 'bottom_bar', '=', '1' ),
			),
			array(
				'id'       => 'bottom_bar_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Bottom Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default bottom right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'bottom_bar_right',
				'required' => array( 'bottom_bar', '=', '1' ),
			),

			array(
				'id'       => 'bottom_bar_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Bottom Bar Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set bottom bar text color.', 'hemelios' ),
				'default'  => '#878787',
				'validate' => 'color',
				'required' => array( 'bottom_bar', '=', '1' ),
			),

			array(
				'id'       => 'bottom_bar_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Bottom Bar Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set bottom bar background color.', 'hemelios' ),
				'default'  => '#111',
				'validate' => 'color',
				'required' => array( 'bottom_bar', '=', '1' ),
			)
		)
	) );