<?php
$g5plus_options = g5plus_option();

$layout_style = g5plus_get_post_meta( $post->ID, 'g5plus_page_layout', true );
if ( ( $layout_style === '' ) || ( $layout_style == '-1' ) ) {
	$layout_style = $g5plus_options['page_layout'];
}

$sidebar = g5plus_get_post_meta( $post->ID, 'g5plus_page_sidebar', true );
if ( ( $sidebar === '' ) || ( $sidebar == '-1' ) ) {
	$sidebar = $g5plus_options['page_sidebar'];
}

$left_sidebar = g5plus_get_post_meta( $post->ID, 'g5plus_page_left_sidebar', true );
if ( ( $left_sidebar === '' ) || ( $left_sidebar == '-1' ) ) {
	$left_sidebar = $g5plus_options['page_left_sidebar'];

}

$right_sidebar = g5plus_get_post_meta( $post->ID, 'g5plus_page_right_sidebar', true );
if ( ( $right_sidebar === '' ) || ( $right_sidebar == '-1' ) ) {
	$right_sidebar = $g5plus_options['page_right_sidebar'];
}

$sidebar_width = g5plus_get_post_meta( $post->ID, 'g5plus_sidebar_width', true );
if ( ( $sidebar_width === '' ) || ( $sidebar_width == '-1' ) ) {
	$sidebar_width = $g5plus_options['page_sidebar_width'];
}


// Calculate sidebar column & content column
$sidebar_col = 'col-md-3';
if ( $sidebar_width == 'large' ) {
	$sidebar_col = 'col-md-4';
}

$content_col_number = 12;
if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'left' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}
if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'right' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}

$content_col = 'col-md-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}


$page_content_mb = g5plus_get_post_meta_box_option( 'g5plus_page_content_mb' );

if ( $page_content_mb == '' || $page_content_mb == '-1' ) {
	if ( isset( $g5plus_options['page_content_mb'] ) ) {
		$page_content_mb = $g5plus_options['page_content_mb'];
	}
}

// Page sidebar bottom
$show_page_bottom_sidebar = g5plus_get_post_meta_box_option( 'g5plus_show_page_bottom_sidebar' );
$page_bottom_sidebar = g5plus_get_post_meta_box_option( 'g5plus_page_bottom_sidebar' );


if ($show_page_bottom_sidebar == '' || $show_page_bottom_sidebar == '-1') {
	if ( isset( $g5plus_options['page_bottom_sidebar'] ) ) {
		$page_bottom_sidebar = $g5plus_options['page_bottom_sidebar'];
	} else {
		$page_bottom_sidebar = '';
	}
}



// Main Class
$main_class = array( 'site-content-page' );

if ( $content_col_number < 12 ) {
	$main_class[] = 'has-sidebar';
}

if ( $page_content_mb == '1' ) {
	$main_class[] = 'mb-bottom';
} elseif ( $page_content_mb == '0' ) {
	$main_class[] = 'no-mb';
}

?>
<?php
/**
 * @hooked - g5plus_page_heading - 5
 **/
do_action( 'g5plus_before_page' );
?>
<main class="<?php echo join( ' ', $main_class ) ?>">
	<?php if ( $layout_style != 'full' ): ?>
	<div class="<?php echo esc_attr( $layout_style ) ?> clearfix">
		<?php endif; ?>

		<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
		<div class="row clearfix">
			<?php endif; ?>

			<?php if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'left' ) || ( $sidebar == 'both' ) ) ): ?>
				<div class="sidebar left-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
					<?php dynamic_sidebar( $left_sidebar ); ?>
				</div>
			<?php endif; ?>
			<div class="site-content-page-inner <?php echo esc_attr( $content_col ) ?>">
				<div class="page-content">
					<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();
						// Include the page content template.
						g5plus_get_template( 'content', 'page' );
					endwhile;
					?>
				</div>
			</div>
			<?php if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'right' ) || ( $sidebar == 'both' ) ) ): ?>

				<div class="sidebar right-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
					<?php dynamic_sidebar( $right_sidebar ); ?>
				</div>
			<?php endif; ?>
			<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
		</div>
	<?php endif; ?>

		<?php if ( $layout_style != 'full' ): ?>
	</div>
<?php endif; ?>
</main>
<?php if ( $show_page_bottom_sidebar != '0' ): ?>
	<?php if ( isset( $page_bottom_sidebar ) && $page_bottom_sidebar != '' ) ?>
	<?php if ( is_active_sidebar( $page_bottom_sidebar ) ): ?>
		<div class="bottom-page sidebar">
			<?php dynamic_sidebar( $page_bottom_sidebar ); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
